import ctypes
import datetime
import math
import os
import pickle
import socket
import threading
import time
import io

import enum
import mathutils
import numpy as np
import pygame
import virtualbox
from pykinect2 import PyKinectV2, PyKinectRuntime
from pykinect2.PyKinectV2 import *

import conf
from basic_data import Point

guest_vm_name = 'baxter_mimic_guest'
guest_user_name = 'bm'
guest_password = 'guest'
shutdown_signal = False


class User_conf:
    def __init__(self):
        self.baxter_hostname = None  # string
        self.host_hostname = None
        self.poi_z = None  # floating point, in meters
        self.poi_radius = None  # floating point, in meters
        self.poi_engage_time = None  # floating point, in seconds
        self.poi_disengage_time = None  # floating point, in seconds
        self.baxter_arm_speed_coef = None  # floating point
        self.spare_time_action_period = None  # floating point,in seconds
        self.game_part_1_duration = None  # floating point, in seconds
        self.game_part_2_duration = None  # floating point, in seconds
        self.auto_shutdown_time = None  # integer, in military time


user_conf = User_conf()


def read_user_configurations():
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)
    file = open("../conf.txt", "r")

    conf_values = []
    for l in file:
        l = l.lstrip()
        l = l.rstrip('\n ')
        if len(l) != 0 and l[0] != '#':
            conf_values.append(l)

    user_conf.baxter_hostname = str(conf_values[0])
    user_conf.poi_z = float(conf_values[1])
    user_conf.poi_radius = float(conf_values[2])
    user_conf.poi_engage_time = float(conf_values[3])
    user_conf.poi_disengage_time = float(conf_values[4])
    user_conf.baxter_arm_speed_coef = float(conf_values[5])
    user_conf.spare_time_action_period = float(conf_values[6])
    user_conf.game_part_1_duration = float(conf_values[7])
    user_conf.game_part_2_duration = float(conf_values[8])
    user_conf.auto_shutdown_time = int(conf_values[9])
    user_conf.host_hostname = socket.gethostname()


class MainController:
    # colors for drawing different bodies
    SKELETON_COLORS = [pygame.color.THECOLORS["red"],
                       pygame.color.THECOLORS["blue"],
                       pygame.color.THECOLORS["green"],
                       pygame.color.THECOLORS["orange"],
                       pygame.color.THECOLORS["purple"],
                       pygame.color.THECOLORS["yellow"],
                       pygame.color.THECOLORS["violet"]]

    class State(enum.Enum):
        GOOD_MORNING = enum.auto()
        INITIALIZE_KINECT = enum.auto()
        INITIALIZE_GUEST = enum.auto()
        WAIT_FOR_FIRST_HEARTBEAT = enum.auto()
        CAPTURE_SEND_DATA = enum.auto()
        RESTART_GUEST = enum.auto()
        TERMINATE_GUEST = enum.auto()
        TERMINATE_KINECT = enum.auto()
        GOOD_BYTE = enum.auto()

    def __init__(self):
        self._done = False
        self._guest_error = False
        self._spawned_guest = False

        self._kinect_running = False
        self._guest_connected = False
        self._guest_heartbeat_OK = False

        self._guest_socket = None
        self._guest_address = None

        self._heartbeat_listener = None

        # Used to manage how fast the screen updates
        self._clock = pygame.time.Clock()

        # Loop until the user clicks the close button.
        self._done = False

        # Used to manage how fast the screen updates
        self._clock = pygame.time.Clock()

        # here we will store skeleton data
        self._bodies = None

        self._client = None

        self.iteration_counter = 0

    def __enter__(self):
        self._heartbeat_listener = threading.Thread(target=self.heartbeat, args=())
        self._heartbeat_listener.start()

        pygame.init()

        # Set the width and height of the screen [width, height]
        self._infoObject = pygame.display.Info()
        self._screen = pygame.display.set_mode((self._infoObject.current_w >> 1, self._infoObject.current_h >> 1),
                                               pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE, 32)

        pygame.display.set_caption("Baxter Mimic Game, Joint Angle Server")

        # Kinect runtime object, we want only color and body frames
        self._kinect = PyKinectRuntime.PyKinectRuntime(
                PyKinectV2.FrameSourceTypes_Color | PyKinectV2.FrameSourceTypes_Body)

        # back buffer surface for getting Kinect color frames, 32bit color, width and height equal to the Kinect color frame size
        self._frame_surface = pygame.Surface(
                (self._kinect.color_frame_desc.Width, self._kinect.color_frame_desc.Height), 0, 32)

        # Create a TCP/IP socket
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.settimeout(5)
        self._socket.bind(("", conf.data_port))

        self._client_listener = threading.Thread(target=self.wait_for_client, args=())
        """@type self._client_listener: threading.Thread"""
        self._client_listener.start()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("Terminating MainController")

        self._done = True
        self._kinect.close()
        self._client_listener.join(60)  # Wait maximum of 1 minute for timeout
        self._socket.close()
        pygame.quit()
        if self._heartbeat_listener is not None and self._heartbeat_listener.is_alive():
            self._heartbeat_listener.join()

        try:
            self.console_session.close()
        except:
            pass
        print("Waiting for guest system to terminate")
        time.sleep(15)  # sleep for 10 seconds giving guest system to prepare shutdown
        self.terminate_vm()
        print("MainController Terminated")
        if exc_val is not None:
            raise exc_val

    def start_vm(self):
        vbox = virtualbox.VirtualBox()
        self.ubuntu_vm = vbox.find_machine(guest_vm_name)
        self.vm_session = None
        self.ubuntu_vm_console = None
        progress = None
        if int(self.ubuntu_vm.state) != 5:  # 5 means machine is running
            self.vm_session = virtualbox.Session()
            # progress = self.ubuntu_vm.launch_vm_process(self.vm_session, 'gui', '')
            progress = self.ubuntu_vm.launch_vm_process(self.vm_session, 'headless', '')
            progress.wait_for_completion(60000)
            if progress.result_code != 0:
                raise Exception("Cannot launch VM")
            self.ubuntu_vm_console = self.vm_session.console
            self._spawned_guest = True
        else:
            self.vm_session = self.ubuntu_vm.create_session()
            self.ubuntu_vm_console = self.vm_session.console
            self._spawned_guest = False

        print("Guest started.")
        if int(self.vm_session.state) < 2:
            raise Exception("Virtual Machine Could not started.")
        time.sleep(2)
        self.console_session = self.ubuntu_vm_console.guest.create_session(guest_user_name, guest_password)
        ret = self.console_session.wait_for(100, 10000)
        print("Guest console session started.")

        # ---------------------------------------------------------------------------------------------------------
        # Wait until device boots
        while True:
            try:
                cs = self.console_session.execute("/bin/ls", [])
                cs[0].wait_for(500, 10000)
                break
            except virtualbox.library_base.VBoxError as e:
                print(e)
                time.sleep(7)

        # ---------------------------------------------------------------------------------------------------------
        # Prepare execution script with tmux
        script_text = "#!/bin/bash\n"
        script_text += 'cd ~\n'
        script_text += 'baxter_hostname="' + user_conf.baxter_hostname + '"\n'
        script_text += 'your_hostname="bmGuest.local"\n'
        script_text += 'source /opt/ros/indigo/setup.bash\n'
        script_text += 'source ros_ws/devel/setup.bash\n'
        script_text += 'export ROS_HOSTNAME="${your_hostname}"\n'
        script_text += 'export ROS_MASTER_URI="http://${baxter_hostname}:11311"\n'
        script_text += 'echo "running baxter_mimic"\n'
        script_text += "tmux new-session -d -s my_session '"
        script_text += 'python /home/bm/baxter_mimic_v2/main.py '
        script_text += str(user_conf.poi_engage_time) + ' '
        script_text += str(user_conf.poi_disengage_time) + ' '
        script_text += str(user_conf.baxter_arm_speed_coef) + ' '
        script_text += str(user_conf.spare_time_action_period) + ' '
        script_text += str(user_conf.game_part_1_duration) + ' '
        script_text += str(user_conf.game_part_2_duration) + ' '
        script_text += user_conf.host_hostname + ' '
        script_text += str(user_conf.poi_z) + ' '
        script_text += str(conf.heartbeat_port) + ' '
        script_text += str(conf.data_port) + ' '
        script_text += ' |& tee output.txt\n'
        script_text += "'"


        script_file = io.open('baxter_mimic.sh', 'w', newline='\n')
        script_file.write(script_text)
        script_file.close()
        print("script baxter_mimic.sh written!.")

        # ----------------------------------------------------------__
        # Prepare execution script wo tmux
        script_text = "#!/bin/bash\n"
        script_text += 'cd ~\n'
        script_text += 'baxter_hostname="' + user_conf.baxter_hostname + '"\n'
        script_text += 'your_hostname="bmGuest.local"\n'
        script_text += 'source /opt/ros/indigo/setup.bash\n'
        script_text += 'source ros_ws/devel/setup.bash\n'
        script_text += 'export ROS_HOSTNAME="${your_hostname}"\n'
        script_text += 'export ROS_MASTER_URI="http://${baxter_hostname}:11311"\n'
        script_text += 'echo "running baxter_mimic"\n'
        script_text += 'python /home/bm/baxter_mimic_v2/baxter_mimic_guest/main.py '
        script_text += str(user_conf.poi_engage_time) + ' '
        script_text += str(user_conf.poi_disengage_time) + ' '
        script_text += str(user_conf.baxter_arm_speed_coef) + ' '
        script_text += str(user_conf.spare_time_action_period) + ' '
        script_text += str(user_conf.game_part_1_duration) + ' '
        script_text += str(user_conf.game_part_2_duration) + ' '
        script_text += user_conf.host_hostname + ' '
        script_text += str(user_conf.poi_z) + ' '
        script_text += str(conf.heartbeat_port) + ' '
        script_text += str(conf.data_port) + ' '
        script_text += ' |& tee output.txt\n'

        script_file = io.open('baxter_mimic_wo.sh', 'w', newline='\n')
        script_file.write(script_text)
        script_file.close()
        print("script baxter_mimic_wo.sh written!.")

        # ----------------------------------------------------------__
        # st = self.console_session.file_copy_to_guest(os.getcwd() + '\\baxter_mimic.sh', '/home/bm/', [])
        # st.wait_for_completion(10000)
        # if st.result_code != 0:
        #     raise Exception("Could not copy script file to Guest!")
        # cs = self.console_session.execute("/bin/chmod", ['u+x', '/home/bm/baxter_mimic.sh'])
        # cs[0].wait_for(500, 10000)
        # if int(cs[0].status) != 500:
        #     raise Exception("Could not give permissions to script file in Guest!")

        # ---------------------------------------------------------------------------------------------------------
        # Execute script
        if self._spawned_guest:
            print("Guest Baxter Mimic process starting...")
            self.p = self.console_session.process_create("/home/bm/baxter_mimic_v2/baxter_host/baxter_mimic.sh", [], [], [], -1)
            self.p.wait_for(1, 10000)

            if int(self.p.status) != 100:
                raise Exception("Guest Baxter Mimic process could not be started.")
            print("Guest Baxter Mimic process started.")
        else:
            print("Guest started by user, script should too!")

    def terminate_vm(self):
        if self._spawned_guest:
            print("Terminating Guest")
            try:
                self.vm_session.console.power_down()
            except:
                pass
            print("Guest power down")
        try:
            self.vm_session.unlock_machine()
        except:
            pass

    def heartbeat(self):
        guest_socket = None
        guest_address = None
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as heartbeat_welcome_socket:
                heartbeat_welcome_socket.bind(("", conf.heartbeat_port))
                heartbeat_welcome_socket.listen(1)
                heartbeat_welcome_socket.settimeout(5)  # All timeouts are 5 seconds

                guest_socket = None

                print("Waiting for guest to connect.")
                while not self._done:
                    try:
                        guest_socket, guest_address = heartbeat_welcome_socket.accept()
                        guest_socket.settimeout(10)
                        print("Guest connected from address " + str(guest_address))
                        break
                    except socket.timeout as t:
                        continue

                if self._done:
                    return

                while not self._done:
                    # Each second send a heartbeat and wait for echo
                    guest_socket.send(b'bu')
                    response = guest_socket.recv(16)
                    if response[:2] == b'mp':
                        self._guest_heartbeat_OK = True
                    else:
                        break
                    time.sleep(5)
        except Exception as e:
            print(e)
            self._guest_error = True
            self._done = True
        finally:
            self._guest_heartbeat_OK = False
            print("Guest heartbeat lost!")
            if guest_socket is not None:
                guest_socket.close()

    def get_kinect_x_limit(self, distance):
        assert (isinstance(distance, (float, int)))
        x = float(distance * math.tan(conf.kinect_FOV / 2))
        return (-x, x)

    @staticmethod
    def aspect_scale(surface, bx, by):
        """ Scales 'img' to fit into box bx/by.
         This method will retain the original image's aspect ratio """
        ix, iy = surface.get_size()
        if ix > iy:
            # fit to width
            scale_factor = bx / float(ix)
            sy = scale_factor * iy
            if sy > by:
                scale_factor = by / float(iy)
                sx = scale_factor * ix
                sy = by
            else:
                sx = bx
        else:
            # fit to height
            scale_factor = by / float(iy)
            sx = scale_factor * ix
            if sx > bx:
                scale_factor = bx / float(ix)
                sx = bx
                sy = scale_factor * iy
            else:
                sy = by

        return pygame.transform.scale(surface, (int(sx), int(sy)))

    @staticmethod
    def translate(value, leftMin, leftMax, rightMin, rightMax):
        # Figure out how 'wide' each range is
        leftSpan = leftMax - leftMin
        rightSpan = rightMax - rightMin

        # Convert the left range into a 0-1 range (float)
        valueScaled = float(value - leftMin) / float(leftSpan)

        # Convert the 0-1 range into a value in the right range.
        return rightMin + (valueScaled * rightSpan)

    @staticmethod
    def kinect_world_to_disp(x, z):
        x_ = int(
                MainController.translate(x, conf.kinect_world_x_max, conf.kinect_world_x_min, 0, conf.draw_people_disp_x))
        y_ = int(
                MainController.translate(z, conf.kinect_world_z_max, conf.kinect_world_z_min, 0, conf.draw_people_disp_y))
        return (x_, y_)

    def disp_to_kinect_world(x, y):
        x_ = MainController.translate(x, 0, conf.draw_people_disp_x,
                                      conf.kinect_world_x_max,
                                      conf.kinect_world_x_min)
        z_ = MainController.translate(y, 0, conf.draw_people_disp_y,
                                      conf.kinect_world_z_max,
                                      conf.kinect_world_z_min)
        return (x_, z_)

    def get_minimap(self):
        # ----------------------------------------------------------------------------------------------------
        # Draw minimap
        draw_people_screen = pygame.Surface((conf.draw_people_disp_x, conf.draw_people_disp_y), 0, 32)
        draw_people_screen.fill((255, 255, 255))

        # Draw shades
        s = pygame.Surface((conf.draw_people_disp_x, conf.draw_people_disp_y))  # the size of your rect
        s.set_alpha(200)  # alpha level
        s.fill((255, 255, 255))

        (kx_min, kx_max) = self.get_kinect_x_limit(conf.kinect_world_z_max)

        baxter_x, baxter_y = MainController.kinect_world_to_disp(0, 0)
        baxter_point = [baxter_x, baxter_y]
        top_left = list(MainController.kinect_world_to_disp(kx_min, conf.kinect_world_z_max))
        bottom_left = list(MainController.kinect_world_to_disp(kx_min, conf.kinect_world_z_min))
        top_right = list(MainController.kinect_world_to_disp(kx_max, conf.kinect_world_z_max))
        bottom_right = list(MainController.kinect_world_to_disp(kx_max, conf.kinect_world_z_min))

        pygame.draw.polygon(s, (0, 0, 0), [baxter_point,
                                           top_left,
                                           bottom_left,
                                           baxter_point,
                                           top_right,
                                           bottom_right,
                                           baxter_point], 0)

        al_tl_x, al_tl_y = MainController.kinect_world_to_disp(conf.person_of_interest_x + user_conf.poi_radius,
                                                               user_conf.poi_z + user_conf.poi_radius)
        al_br_x, al_br_y = MainController.kinect_world_to_disp(conf.person_of_interest_x - user_conf.poi_radius,
                                                               user_conf.poi_z - user_conf.poi_radius)

        pygame.draw.rect(s, (0, 100, 200), pygame.Rect(al_tl_x, al_tl_y, al_br_x - al_tl_x, al_br_y - al_tl_y),
                         2)
        draw_people_screen.blit(s, [0, 0])

        red_line_disp_y = int(
                MainController.translate(conf.kinect_person_z_min, conf.kinect_world_z_max, conf.kinect_world_z_min, 0,
                                         conf.draw_people_disp_y))
        pygame.draw.line(draw_people_screen, (200, 20, 20), [0, red_line_disp_y],
                         [conf.draw_people_disp_x, red_line_disp_y], 2)

        return draw_people_screen

    def filter_joint_angle(self, joint_angle):
        average_list_size = 20
        angle_outlier_threshold = 1.0

        def reject_outliers(data, m=2.):
            d = np.abs(data - np.median(data))
            mdev = np.median(d)
            s = d / mdev if mdev else 0.
            return data[s < m]

        left, right = joint_angle

        if left is None or right is None:
            self._left_joint_angle_list = []
            self._right_joint_angle_list = []
            return

        self._left_joint_angle_list.append(left)
        if len(self._left_joint_angle_list) > average_list_size:
            self._left_joint_angle_list.pop(0)

        self._right_joint_angle_list.append(right)
        if len(self._right_joint_angle_list) > average_list_size:
            self._right_joint_angle_list.pop(0)

        if len(self._left_joint_angle_list) == 1:
            l_average = self._left_joint_angle_list[0]
        else:
            l_average = dict()
            for key in self._left_joint_angle_list[0].keys():
                cleaned_list = reject_outliers(np.array([mem[key] for mem in self._left_joint_angle_list]),
                                               angle_outlier_threshold)
                l_average[key] = float(np.mean(cleaned_list))

        if len(self._right_joint_angle_list) == 1:
            r_average = self._right_joint_angle_list[0]
        else:
            r_average = dict()
            for key in self._right_joint_angle_list[0].keys():
                cleaned_list = reject_outliers(np.array([mem[key] for mem in self._right_joint_angle_list]),
                                               angle_outlier_threshold)
                r_average[key] = float(np.mean(cleaned_list))
        return l_average, r_average

    def extract_and_print_joint_angles(self, jointOrientations, joints, jointPoints):
        '''
            @type joints: list[pykinect2.PyKinectV2._Joint]
            @type jointOrientations: list[pykinect2.PyKinectV2._JointOrientation]

            @returns dict[str,float]
        '''

        joint_list = [JointType_SpineBase, JointType_SpineMid, JointType_SpineShoulder,  # 0
                      JointType_Neck, JointType_Head,  # 3
                      JointType_ShoulderLeft, JointType_ElbowLeft, JointType_WristLeft, JointType_HandLeft,  # 5
                      JointType_ShoulderRight, JointType_ElbowRight, JointType_WristRight, JointType_HandRight,  # 9
                      JointType_HipLeft, JointType_KneeLeft, JointType_AnkleLeft, JointType_FootLeft,  # 13
                      JointType_HipRight, JointType_KneeRight, JointType_AnkleRight, JointType_FootRight,  # 17
                      JointType_HandTipLeft, JointType_ThumbLeft,  # 21
                      JointType_HandTipRight, JointType_ThumbRight]  # 23

        joint_rotation_array = [mathutils.Quaternion((jointOrientations[frame].Orientation.w,
                                                      jointOrientations[frame].Orientation.x,
                                                      jointOrientations[frame].Orientation.y,
                                                      jointOrientations[frame].Orientation.z)) for frame in
                                range(0, JointType_Count)]
        ''' @type joint_rotation_array: list[mathutils.Quaternion] '''

        # orient reference frame
        elbow_normal_rotation = joint_rotation_array[JointType_SpineShoulder] * mathutils.Quaternion((1, 0, 0),
                                                                                                     -math.pi / 2) * mathutils.Quaternion(
                (0, 0, 1), -math.pi / 2)
        ''' @type elbow_normal_rotation: mathutils.Quaternion '''
        joint_rotation_array[JointType_SpineShoulder] = elbow_normal_rotation

        # -------------------------------------------------------
        ##LEFT ARM
        joint_rotation_array[JointType_ShoulderLeft] = joint_rotation_array[JointType_ElbowLeft] * mathutils.Quaternion(
                (1, 0, 0), math.pi / 2) * mathutils.Quaternion((0, 1, 0), math.pi / 2)
        elbow_left_relative_to_normal_rotation = elbow_normal_rotation.inverted() * joint_rotation_array[
            JointType_ShoulderLeft]
        ''' @type elbow_left_relative_to_normal_rotation: mathutils.Quaternion '''
        elbow_rot_matrix = elbow_left_relative_to_normal_rotation.to_matrix()

        ###INVERSE KINEMATICS
        left_S0 = math.atan2(elbow_rot_matrix[1][0], elbow_rot_matrix[0][0])  # Z
        left_E0 = math.atan2(elbow_rot_matrix[2][1], elbow_rot_matrix[2][2])  # X
        try:
            left_S1 = math.atan2(-elbow_rot_matrix[2][0], elbow_rot_matrix[1][0] / math.sin(left_S0))  # Y
        except ZeroDivisionError:
            left_S1 = 0

        ##LEFT FOREARM
        wrist_normal_rotation = joint_rotation_array[JointType_ShoulderLeft]
        joint_rotation_array[JointType_ElbowLeft] = joint_rotation_array[JointType_WristLeft] * mathutils.Quaternion(
                (1, 0, 0), math.pi / 2) * mathutils.Quaternion((0, 1, 0), math.pi / 2)
        wrist_left_relative_to_normal_rotation = wrist_normal_rotation.inverted() * joint_rotation_array[
            JointType_ElbowLeft]
        ''' @type elbow_left_relative_to_normal_rotation: mathutils.Quaternion '''
        wrist_rot_matrix = wrist_left_relative_to_normal_rotation.to_matrix()

        left_E1 = -math.atan2(-wrist_rot_matrix[2][0], wrist_rot_matrix[0][0])  # Y
        left_W0 = math.atan2(-wrist_rot_matrix[1][2], wrist_rot_matrix[1][1])  # X

        # -------------------------------------------------------
        ##RIGHT ARM
        joint_rotation_array[JointType_ShoulderRight] = joint_rotation_array[
                                                            JointType_ElbowRight] * mathutils.Quaternion((1, 0, 0),
                                                                                                         -math.pi / 2) * mathutils.Quaternion(
                (0, 1, 0), -math.pi / 2)
        elbow_right_relative_to_normal_rotation = elbow_normal_rotation.inverted() * joint_rotation_array[
            JointType_ShoulderRight]
        ''' @type elbow_right_relative_to_normal_rotation: mathutils.Quaternion '''
        elbow_rot_matrix = elbow_right_relative_to_normal_rotation.to_matrix()

        ###INVERSE KINEMATICS
        right_S0 = math.atan2(elbow_rot_matrix[1][0], elbow_rot_matrix[0][0])
        right_E0 = math.atan2(elbow_rot_matrix[2][1], elbow_rot_matrix[2][2])
        try:
            right_S1 = math.atan2(-elbow_rot_matrix[2][0], elbow_rot_matrix[1][0] / math.sin(right_S0))
        except ZeroDivisionError:
            right_S1 = 0

        # RIGHT FOREARM
        wrist_normal_rotation = joint_rotation_array[JointType_ShoulderRight]
        joint_rotation_array[JointType_ElbowRight] = joint_rotation_array[JointType_WristRight] * mathutils.Quaternion(
                (1, 0, 0), -math.pi / 2) * mathutils.Quaternion((0, 1, 0), -math.pi / 2)
        wrist_right_relative_to_normal_rotation = wrist_normal_rotation.inverted() * joint_rotation_array[
            JointType_ElbowRight]
        ''' @type elbow_left_relative_to_normal_rotation: mathutils.Quaternion '''
        wrist_rot_matrix = wrist_right_relative_to_normal_rotation.to_matrix()

        right_E1 = -math.atan2(-wrist_rot_matrix[2][0], wrist_rot_matrix[0][0])  # Y
        right_W0 = math.atan2(-wrist_rot_matrix[1][2], wrist_rot_matrix[1][1])  # X

        joint_print_list = [JointType_SpineShoulder,
                            JointType_ShoulderLeft, JointType_ElbowLeft,
                            JointType_ShoulderRight, JointType_ElbowRight]

        try:
            for joint in joint_print_list:  # [JointType_SpineShoulder,JointType_ShoulderLeft,JointType_ShoulderRight]:
                self.paint_frame(joint, joints[joint].Position, joint_rotation_array[joint], jointPoints[joint])
        except:
            pass

        angles = ({'left_s0'.encode('utf-8'): left_S0 - math.pi / 4,
                   'left_s1'.encode('utf-8'): left_S1,
                   'left_e0'.encode('utf-8'): left_E0 - math.pi,
                   'left_e1'.encode('utf-8'): left_E1,
                   'left_w0'.encode('utf-8'): left_W0,
                   'left_w1'.encode('utf-8'): 0,
                   'left_w2'.encode('utf-8'): 0},
                  {'right_s0'.encode('utf-8'): right_S0 + math.pi / 4,
                   'right_s1'.encode('utf-8'): right_S1,
                   'right_e0'.encode('utf-8'): right_E0 + math.pi,
                   'right_e1'.encode('utf-8'): right_E1,
                   'right_w0'.encode('utf-8'): right_W0,
                   'right_w1'.encode('utf-8'): 0,
                   'right_w2'.encode('utf-8'): 0})

        return angles

    def run(self):
        self.start_vm()

        self.iteration_counter = 0
        self.filter_joint_angle((None, None))
        # -------- Main Program Loop -----------
        while not self._done:
            # win32gui.PumpWaitingMessages()
            cur_time = datetime.datetime.now().time()
            if user_conf.auto_shutdown_time <= ((cur_time.hour * 100) + cur_time.minute):
                self._done = True
                print("Auto shutdown is activated!")
                continue

            # global shutdown_signal
            # if shutdown_signal:
            #     self._done = True
            #     continue
            # --- Main event loop
            for event in pygame.event.get():  # User did something
                if event.type == pygame.QUIT:  # If user clicked close
                    self._done = True  # Flag that we are done so we exit this loop

                elif event.type == pygame.VIDEORESIZE:  # window resized
                    self._screen = pygame.display.set_mode(event.dict['size'],
                                                           pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE,
                                                           32)

            # --- Game logic should go here

            # --- Getting frames and drawing
            # --- Woohoo! We've got a color frame! Let's fill out back buffer surface with frame's data
            if self._kinect.has_new_color_frame():
                frame = self._kinect.get_last_color_frame()
                self.draw_color_frame(frame, self._frame_surface)
                frame = None

            # --- Cool! We have a body frame, so can get skeletons
            if self._kinect.has_new_body_frame():
                self._bodies = self._kinect.get_last_body_frame()

            # if self._kinect.has_new_depth_frame():
            #     self._depth=self._kinect.get_last_depth_frame()

            # --- draw skeletons to _frame_surface

            body_on_spot = None
            people_coordinates = []
            new_joint_angles = None
            average_joint_angle = None
            if self._bodies is not None:
                for i in range(0, self._kinect.max_body_count):
                    if self._bodies.bodies[i].is_tracked:
                        spine_shoulder_pos = self._bodies.bodies[i].joints[JointType_SpineShoulder].Position
                        pos_x = spine_shoulder_pos.x
                        pos_y = spine_shoulder_pos.y
                        pos_z = spine_shoulder_pos.z

                        # print("%3.2f, %3.2f, %3.2f" % (pos_x, pos_y, pos_z))
                        if (-user_conf.poi_radius <= pos_x - conf.person_of_interest_x <= user_conf.poi_radius) \
                                and (-user_conf.poi_radius <= pos_z - user_conf.poi_z <= user_conf.poi_radius):
                            body_on_spot = self._bodies.bodies[i]
                        p = Point(pos_x, pos_y, pos_z)
                        p.color = (0, 0, 0)
                        people_coordinates.append(p)

                if body_on_spot is not None and body_on_spot.is_tracked:
                    joints = body_on_spot.joints
                    # convert joint coordinates to color space
                    joint_points = self._kinect.body_joints_to_color_space(joints)
                    self.draw_body(joints, joint_points, self.SKELETON_COLORS[0])

                    jointOrientations = body_on_spot.joint_orientations
                    new_joint_angles = self.extract_and_print_joint_angles(jointOrientations, joints, joint_points)

                    average_joint_angle = self.filter_joint_angle(new_joint_angles)

                    # ----------------------------------
            if self._client is not None and self.iteration_counter % 3 == 0:
                f = self._client[0].makefile('wb', 600)
                try:
                    pickle.dump((average_joint_angle, None, people_coordinates), f, protocol=2)
                    if self.iteration_counter % 15:
                        print((average_joint_angle, None, people_coordinates))
                    f.close()
                except Exception as e:
                    print(e)
                    self._client[0].shutdown(socket.SHUT_RDWR)
                    self._client[0].close()
                    self._client = None

            if self.iteration_counter % 2 == 0:
                # ----------------------------------------------------------------------------------------------------
                draw_people_screen = self.get_minimap()
                for p in people_coordinates:
                    pygame.draw.circle(draw_people_screen, p.color, list(MainController.kinect_world_to_disp(p.x, p.z)),
                                       10)

                # ----------------------------------------------------------------------------------------------------
                # --- copy back buffer surface pixels to the screen, resize it if needed and keep aspect ratio
                # --- (screen size may be different from Kinect's color frame size)
                h_to_w = float(self._frame_surface.get_height()) / self._frame_surface.get_width()
                target_height = int(h_to_w * self._screen.get_width())
                surface_to_draw = self.aspect_scale(self._frame_surface, self._screen.get_width(),
                                                    self._screen.get_height())
                surface_to_draw = pygame.transform.flip(surface_to_draw, True, False)

                # ----------------------------------------------------------------------------------------------------
                self._screen.blit(surface_to_draw, (0, 0))

                draw_people_screen = self.aspect_scale(draw_people_screen, self._screen.get_width() * 0.25,
                                                       self._screen.get_height() * 0.25)
                self._screen.blit(draw_people_screen, (0, 0))
                # self._screen.blit(draw_people_screen, (int(self._screen.get_width() * 0.75), int(target_height * 0.75)))
                # --- Go ahead and update the screen with what we've drawn.
                pygame.display.update()
                pygame.display.flip()

            self.iteration_counter += 1
            self._clock.tick(30)

    # -------------------------------------------------------------------------

    def wait_for_client(self):
        self._socket.listen(1)
        while not self._done:
            try:
                if self._client is None:
                    clientsocket, addr = self._socket.accept()
                    self._client = (clientsocket, addr)
                else:
                    time.sleep(5)
            except socket.timeout as t:
                continue

    # -------------------------------------------------------------------------

    def draw_body_bone(self, joints, jointPoints, color, joint0, joint1):

        if joints[joint0].TrackingState is PyKinectV2.TrackingState_NotTracked or \
                        joints[joint1].TrackingState is PyKinectV2.TrackingState_NotTracked:
            return

        joint0State = joints[joint0].TrackingState
        joint1State = joints[joint1].TrackingState

        # both joints are not tracked
        if (joint0State == PyKinectV2.TrackingState_NotTracked) or (joint1State == PyKinectV2.TrackingState_NotTracked):
            return

        # both joints are not *really* tracked
        if (joint0State == PyKinectV2.TrackingState_Inferred) and (joint1State == PyKinectV2.TrackingState_Inferred):
            return

        # ok, at least one is good
        start = (jointPoints[joint0].x, jointPoints[joint0].y)
        end = (jointPoints[joint1].x, jointPoints[joint1].y)

        try:
            pygame.draw.line(self._frame_surface, color, start, end, 4)
        except:  # need to catch it due to possible invalid positions (with inf)
            pass

    # -------------------------------------------------------------------------

    def draw_body(self, joints, jointPoints, color):
        # Torso
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_Head, PyKinectV2.JointType_Neck)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_Neck, PyKinectV2.JointType_SpineShoulder)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineShoulder,
                            PyKinectV2.JointType_SpineMid)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineMid, PyKinectV2.JointType_SpineBase)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineShoulder,
                            PyKinectV2.JointType_ShoulderRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineShoulder,
                            PyKinectV2.JointType_ShoulderLeft)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineBase, PyKinectV2.JointType_HipRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineBase, PyKinectV2.JointType_HipLeft)

        # Right Arm
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_ShoulderRight,
                            PyKinectV2.JointType_ElbowRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_ElbowRight,
                            PyKinectV2.JointType_WristRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_WristRight,
                            PyKinectV2.JointType_HandRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_HandRight,
                            PyKinectV2.JointType_HandTipRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_WristRight,
                            PyKinectV2.JointType_ThumbRight)

        # Left Arm
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_ShoulderLeft,
                            PyKinectV2.JointType_ElbowLeft)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_ElbowLeft, PyKinectV2.JointType_WristLeft)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_WristLeft, PyKinectV2.JointType_HandLeft)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_HandLeft,
                            PyKinectV2.JointType_HandTipLeft)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_WristLeft, PyKinectV2.JointType_ThumbLeft)

        # Right Leg
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_HipRight, PyKinectV2.JointType_KneeRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_KneeRight,
                            PyKinectV2.JointType_AnkleRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_AnkleRight,
                            PyKinectV2.JointType_FootRight)

        # Left Leg
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_HipLeft, PyKinectV2.JointType_KneeLeft)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_KneeLeft, PyKinectV2.JointType_AnkleLeft)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_AnkleLeft, PyKinectV2.JointType_FootLeft)

    # -------------------------------------------------------------------------
    def draw_color_frame(self, frame, target_surface):
        target_surface.lock()
        address = self._kinect.surface_as_array(target_surface.get_buffer())
        ctypes.memmove(address, frame.ctypes.data, frame.size)
        del address
        target_surface.unlock()

    # -------------------------------------------------------------------------
    def paint_frame(self, joint_type, joint_camera_position, joint_camera_rotation, joint_color_position):
        '''
            @type joint_type: int
            @type joint_camera_position: PyKinectV2._CameraSpacePoint
            @type joint_camera_rotation: mathutils.Quaternion
            @type joint_color_position: PyKinectV2._ColorSpacePoint
        '''
        div_val = 15

        rot_matrix = joint_camera_rotation.to_matrix()

        frame_endpoints = []
        for i in range(0, 3):
            cam_space_coordinate = PyKinectV2._CameraSpacePoint()
            cam_space_coordinate.x = rot_matrix[0][i] / div_val + joint_camera_position.x
            cam_space_coordinate.y = rot_matrix[1][i] / div_val + joint_camera_position.y
            cam_space_coordinate.z = rot_matrix[2][i] / div_val + joint_camera_position.z
            col_space_coordinate = self._kinect._mapper.MapCameraPointToColorSpace(cam_space_coordinate)
            frame_endpoints.append(col_space_coordinate)

        frame_colors = [pygame.color.THECOLORS["red"], pygame.color.THECOLORS["blue"], pygame.color.THECOLORS["green"]]

        for i in range(0, 3):
            start = (joint_color_position.x, joint_color_position.y)
            end = (frame_endpoints[i].x, frame_endpoints[i].y)
            pygame.draw.line(self._frame_surface, frame_colors[i], start, end, 5)


# oldWndProc=None
# def wndproc(hWnd, msg, wParam, lParam):
#     print("wndproc received message: %s" % msg)
#     if msg in [win32con.WM_QUERYENDSESSION, win32con.WM_ENDSESSION]:
#         global shutdown_signal
#         shutdown_signal=True
#         return 0
#     # Pass all messages to the original WndProc
#     return win32gui.CallWindowProc(oldWndProc, hWnd, msg, wParam, lParam)
#
# ready_to_shutdown=False
# def prevent_shutdown():
#     try:
#         pass
#         # Create a window just to be able to receive WM_QUERYENDSESSION messages
#         win32api.SetLastError(0)
#         hinst = win32api.GetModuleHandle(None)
#         messageMap = {}
#         wndclass = win32gui.WNDCLASS()
#         wndclass.hInstance = hinst
#         wndclass.lpszClassName = "PreventShutdownWindowClass"
#         wndclass.lpfnWndProc = messageMap
#         myWindowClass = win32gui.RegisterClass(wndclass)
#         hwnd = win32gui.CreateWindowEx(win32con.WS_EX_LEFT,
#                                        myWindowClass,
#                                        "PreventShutdownWindow",
#                                        0,
#                                        0,
#                                        0,
#                                        win32con.CW_USEDEFAULT,
#                                        win32con.CW_USEDEFAULT,
#                                        0,
#                                        0,
#                                        hinst,
#                                        None)
#         print('CreateWindowEx: ' + str(hwnd))
#         print('CreateWindowEx last error: ' + str(win32api.GetLastError()))
#         print('')
#
#         # # Set WndProc
#         # win32api.SetLastError(0)
#         # WndProcType = ctypes.WINFUNCTYPE(c_int, c_long, c_int, c_int, c_int)
#         # newWndProc = WndProcType(wndproc)
#         # oldWndProc = ctypes.windll.user32.SetWindowLongW(hwnd, win32con.GWL_WNDPROC, newWndProc)
#         # print('SetWindowLong: ' + str(oldWndProc))
#         # print('SetWindowLong last error: ' + str(win32api.GetLastError()))
#         # print('')
#         #
#         # # We provide a reason for the shutdown prevention
#         # win32api.SetLastError(0)
#         # ret = ctypes.windll.user32.ShutdownBlockReasonCreate(hwnd, c_wchar_p(
#         #     "PreventShutdown running which prevents shutdown"))
#         # print('ShutdownBlockReasonCreate: ' + str(ret))
#         # print('ShutdownBlockReasonCreatelast error: ' + str(win32api.GetLastError()))
#         # print('')
#         #
#         # # We elevate the program to be asked as soon as possible to inhibit shutdown
#         # win32api.SetLastError(0)
#         # win32process.SetProcessShutdownParameters(0x4FF, win32con.SHUTDOWN_NORETRY)
#         # print('SetProcessShutdownParameters last error: ' + str(win32api.GetLastError()))
#         # print("")
#     except Exception as e:
#        raise e
#



if __name__ == '__main__':
    print(str(datetime.datetime.now()))
    __main__ = "Baxter Mimic Game, Joint Angle Server"

    read_user_configurations()
    # user_conf.baxter_hostname = 'fubuntuPC.local'
    # user_conf.auto_shutdown_time = 9999
    # prevent_shutdown()
    while True:
        with MainController() as m:
            m.run()
            if not m._guest_error:
                break
    ready_to_shutdown = True
    # os.system('shutdown -s')
    #
    # try:
    #     __main__ = "Baxter Mimic Game, Joint Angle Server"
    #     main = MainController()
    #     main.run()
    # except:
    #     type, value, tb = sys.exc_info()
    #     traceback.print_exc()
    #     last_frame = lambda tb=tb: last_frame(tb.tb_next) if tb.tb_next else tb
    #     frame = last_frame().tb_frame
    #     ns = dict(frame.f_globals)
    #     ns.update(frame.f_locals)
    #     code.interact(local=ns)
