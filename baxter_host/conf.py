heartbeat_port = 10000
data_port = 10005

person_of_interest_x = 0  # center

# Kinect and Environment seen by kinect
kinect_world_x_min = -6.0
kinect_world_x_max = 6.0

kinect_person_z_min = 2.0
kinect_world_z_min = 0.0
kinect_world_z_max = 8.0

kinect_FOV = 70.6

draw_people_disp_x = int(50 * abs(kinect_world_x_max - kinect_world_x_min))
draw_people_disp_y = int(50 * abs(kinect_world_z_max - kinect_world_z_min))
