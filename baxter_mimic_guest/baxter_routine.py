import copy
import random
import threading
import time

import enum
from pygame import mixer

import behaviors
import conf
from action.actions import Action
import shared_data
from my_baxter import My_baxter, Baxter_data
from my_kinect import My_kinect, Kinect_data


class States(enum.Enum):
    GOOD_MORNING = 0
    SPENDING_TIME = 1
    WAITING_USER_TO_WAIT_ON_SPOT = 2
    EXPLAINING_GAME = 3
    GAME_PART_1 = 4
    PREPARE_FOR_PART_2 = 5
    GAME_PART_2 = 6
    SAY_GOOD_BYE = 7
    LEAVING_PERSON = 8
    GOOD_NIGHT = 9


class Baxter_routine(object):
    """
    :type _baxter:My_baxter
    """

    # ----------------------------------------------


    # ----------------------------------------------

    def __init__(self, baxter, kinect):
        assert (isinstance(baxter, My_baxter))
        assert (isinstance(kinect, My_kinect))
        self._done = False

        self._baxter = baxter
        self._kinect = kinect
        # ---------------------------------------------


        # --------------------------------------------
        self._actions_lock = threading.Lock()
        self._actions_routine = None
        self._actions_val = None
        self._actions_new = False

        # ---------------------------------------------
        self._terminate_lock = threading.RLock()
        self._terminate_condition = threading.Condition(self._terminate_lock)

        # ---------------------------------------------
        self._spending_time_actions = None
        # --------------------------------------------
        mixer.init()

        self.action_server_period = 0.1  # seconds

        self.counter = 0
        self._state = States.GOOD_MORNING

    def __enter__(self):

        # ----------------------------------------------------------------------------------
        # Action_step Servers Initialization
        # self._arm_action_routine = threading.Thread(target=self.arm_action_server, args=())
        # self._head_action_routine = threading.Thread(target=self.head_action_server, args=())
        # self._speech_action_routine = threading.Thread(target=self.speech_action_server, args=())
        # -----------------------------------------------------------------------------------
        # self._arm_action_routine.start()
        # self._head_action_routine.start()
        # self._speech_action_routine.start()


        self._action_routine = threading.Thread(target=self.action_server, args=(), name="Action_server")
        # -----------------------------------------------------------------------------------
        self._behavior_routine = threading.Thread(target=self.behavior_routine, args=(), name="Behavior_routine")

        self._action_routine.start()
        # -----------------------------------------------------------------------------
        self._behavior_routine.start()

        # -----------------------------------------------------------------------------
        self._spending_time_actions = behaviors.get_spending_time_random_actions(20,
                                                                                 conf.spare_time_action_period)

        # -----------------------------------------------------------------------------

        shared_data.ready_baxter_routine = True
        print("Baxter_routine ready!")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        shared_data.done = True
        print("Baxter_routine terminating!")
        #
        # if self._arm_action_routine is not None and self._arm_action_routine.is_alive():
        #     self._arm_action_routine.join()
        # if self._head_action_routine is not None and self._head_action_routine.is_alive():
        #     self._head_action_routine.join()
        # if self._speech_action_routine is not None and self._speech_action_routine.is_alive():
        #     self._speech_action_routine.join()

        if self._behavior_routine is not None and self._behavior_routine.is_alive():
            self._behavior_routine.join()

        if self._action_routine is not None and self._action_routine.is_alive():
            self._action_routine.join()
        shared_data.done_baxter_routine = True
        print("Baxter_routine terminated!")
        if exc_val is not None:
            raise exc_val

    def set_action(self, action):
        assert (isinstance(action, Action))
        if self._actions_lock.acquire(True):
            try:
                self._actions_val = action
            finally:
                self._actions_lock.release()

    def get_action(self):
        '''
        :rtype:Action | None
        '''
        action = None
        if self._actions_lock.acquire(True):
            try:
                action = self._actions_val
            finally:
                self._actions_lock.release()
        return action

    def action_server(self):
        self._playing_speech_file = None
        self._channel = None
        next_iteration_time = time.time()

        def update_speech(speech_file):
            if speech_file is not None:
                assert (isinstance(speech_file, str))

            def play():
                self._channel = mixer.Sound(speech_file).play()
                self._playing_speech_file = speech_file

            def stop():
                mixer.stop()
                self._playing_speech_file = None
                self._channel = None

            if speech_file is None:
                stop()
                return
            if self._channel is None:
                play()
            elif speech_file != self._playing_speech_file:
                stop()
                play()
            elif speech_file == self._playing_speech_file and not self._channel.get_busy():
                play()

        counter = 0
        while not shared_data.done:
            next_iteration_time += self.action_server_period
            cur_action = self.get_action()

            if cur_action is not None:
                counter += 1
                cur_action.is_speech_action_ready()
                cur_action.is_head_action_ready()
                cur_action.is_arm_action_ready()

                kinect_data = self._kinect.get_kinect_data()
                baxter_data = self._baxter.get_baxter_data()
                assert (isinstance(kinect_data, Kinect_data))
                assert (isinstance(baxter_data, Baxter_data))

                if counter % 4 == 0:
                    # Speech
                    new_data = cur_action.speech_get_new_values(kinect_data, baxter_data)
                    # self._infodisplay.set_draw_engage_point_speech(cur_action.get_speech_focus_point_current())
                    if new_data is not None:
                        update_speech(new_data)
                    else:
                        update_speech(None)

                if True:  # counter % 1 == 0:
                    # Arm
                    new_data = cur_action.arm_get_new_values(kinect_data, baxter_data)
                    # self._infodisplay.set_draw_engage_point_arm(cur_action.get_arm_focus_point_current())
                    if new_data is not None:
                        self._baxter.update_arm_speed(*new_data)
                    else:
                        self._baxter.update_arm_speed(None, None)

                if counter % 4 == 0:
                    # Head
                    new_data = cur_action.head_get_new_values(kinect_data, baxter_data)
                    # self._infodisplay.set_draw_engage_point_head(cur_action.get_head_focus_point_current())
                    if new_data is not None:
                        self._baxter.update_head(*new_data)
                    else:
                        self._baxter.update_head(None, None, None)

            else:
                update_speech(None)
                self._baxter.update_head(None, None, None)
                self._baxter.update_arm_speed(None, None)
                # counter = 0

            time.sleep(max(0, next_iteration_time - time.time()))

    # def arm_action_server(self):
    #
    #     # ------------------------------------------------------------------------------------------------------
    #     ''':type :list[Action_step] | None '''
    #     while not shared_data.done:
    #         cur_action = self.get_action()
    #         if cur_action is not None and cur_action.is_arm_action_ready():
    #             baxter_data = self._baxter.get_baxter_data(False)
    #             kinect_data = self._kinect.get_kinect_data(False)
    #             new_data = cur_action.arm_get_new_values(kinect_data, baxter_data)
    #             # self._infodisplay.set_draw_engage_point_arm(cur_action.get_arm_focus_point_current())
    #             if new_data is not None:
    #                 self._baxter.update_arm_speed(*new_data)
    #             else:
    #                 self._baxter.update_arm_speed(None, None)
    #         else:
    #             self._baxter.update_arm_speed(None, None)
    #         time.sleep(0.1)
    #
    # def head_action_server(self):
    #     # ------------------------------------------------------------------------------------------------------
    #     ''':type :list[Action_step] | None '''
    #     while not shared_data.done:
    #         cur_action = self.get_action()
    #         if cur_action is not None and cur_action.is_head_action_ready():
    #             baxter_data = self._baxter.get_baxter_data(False)
    #             kinect_data = self._kinect.get_kinect_data(False)
    #             new_data = cur_action.head_get_new_values(kinect_data, baxter_data)
    #             # self._infodisplay.set_draw_engage_point_head(cur_action.get_head_focus_point_current())
    #             if new_data is not None:
    #                 self._baxter.update_head(*new_data)
    #             else:
    #                 self._baxter.update_head(None, None, None)
    #         else:
    #             self._baxter.update_head(None, None, None)
    #         time.sleep(0.1)
    #
    # def speech_action_server(self):
    #
    #     # ------------------------------------------------------------------------------------------------------
    #     ''':type :list[Action_step] | None '''
    #     while not shared_data.done:
    #         cur_action = self.get_action()
    #         if cur_action is not None and cur_action.is_speech_action_ready():
    #             baxter_data = self._baxter.get_baxter_data(False)
    #             kinect_data = self._kinect.get_kinect_data(False)
    #             new_data = cur_action.speech_get_new_values(kinect_data, baxter_data)
    #             # self._infodisplay.set_draw_engage_point_speech(cur_action.get_speech_focus_point_current())
    #             if new_data is not None:
    #                 update_speech(new_data)
    #             else:
    #                 update_speech(None)
    #         else:
    #             update_speech(None)
    #         time.sleep(0.1)

    def behavior_routine(self):
        # ---------------------------------------------------_
        g_good_morning = None
        g_good_night = None
        g_spending_time = None
        g_explain_game = None
        g_game_part_1 = None
        g_prepare_part_2 = None
        g_game_part_2 = None
        g_leaving_person = None
        g_say_good_bye = None

        self._waiting_start_time = None
        self._leaving_start_time = None

        # ---------------------------------------------------_
        def has_person_left():
            '''
            :rtype:bool
            '''
            # user detected left spot
            if self._kinect.get_kinect_data().pos_ja is not None:
                return False
            ctime = time.time()
            while (time.time() - ctime > conf.disengage_duration):
                if self._kinect.get_kinect_data().pos_ja is not None:
                    return False
                time.sleep(0.5)
            return True


        # ---------------------------------------------------_
        def has_person_arrived():
            '''
            :rtype:bool
            '''

            if self._kinect.get_kinect_data().pos_ja is None:
                return False
            ctime = time.time()
            while time.time() - ctime < conf.engage_duration:
                if self._kinect.get_kinect_data().pos_ja is None:
                    return False
                time.sleep(0.5)
            return True

            # if kinect_data is not None and kinect_data.pos_ja is not None:
            #     if self._waiting_start_time is None:
            #         self._waiting_start_time = time.time()
            #     if time.time() - self._waiting_start_time > conf.engage_duration:
            #         self._waiting_start_time = None
            #         return True
            #     else:
            #         return False
            # else:
            #     self._waiting_start_time = None
            #     return None

        # ---------------------------------------------------_
        while not shared_data.done:
            # if self.counter % 2 == 0:
            #     print(self._kinect.get_kinect_data().pos_ja)
            #     print(self._kinect.get_kinect_data().people)
            self.counter += 1
            # Holly State Machine

            if self._done:
                self._state = States.GOOD_NIGHT

            # --------------------------------------------------------------------------------------------
            if States.GOOD_MORNING == self._state:
                if g_good_morning is None:
                    g_good_morning = self.good_morning()

                try:
                    if not g_good_morning.next():
                        g_good_morning = None
                        self._state = States.SPENDING_TIME
                except StopIteration:
                    g_good_morning = None


            # --------------------------------------------------------------------------------------------
            elif States.LEAVING_PERSON == self._state:

                if g_leaving_person is None:
                    g_leaving_person = self.leaving_person()

                try:
                    if not g_leaving_person.next():
                        g_leaving_person = None
                        self._state = States.SPENDING_TIME
                except StopIteration:
                    assert (False)
                    g_leaving_person = None
                    self._state = States.SPENDING_TIME

            # --------------------------------------------------------------------------------------------
            elif self._state in [States.SPENDING_TIME, States.WAITING_USER_TO_WAIT_ON_SPOT]:

                if g_spending_time is None:
                    g_spending_time = self.spend_time()

                try:
                    if not g_spending_time.next():
                        g_spending_time = None  # We need a new action
                except StopIteration:
                    assert (False)
                    g_spending_time = None

                # user detected on spot
                hpa = has_person_arrived()

                if hpa is True:
                    g_spending_time = None
                    self._state = States.EXPLAINING_GAME
                elif hpa is False:  # Person not arrived
                    self._state = States.WAITING_USER_TO_WAIT_ON_SPOT
                else:  # if hpa is None means user on spot but necessary time has not reached yet.
                    self._state = States.SPENDING_TIME


            # --------------------------------------------------------------------------------------------
            elif States.EXPLAINING_GAME == self._state:
                # Greet the player
                # Explain the game

                if g_explain_game is None:
                    g_explain_game = self.explain_game()

                try:
                    if not g_explain_game.next():
                        g_explain_game = None
                        self._state = States.GAME_PART_1
                except StopIteration:
                    assert (False)
                    g_explain_game = None
                    self._state = States.SPENDING_TIME

                hpa = has_person_left()
                if hpa is True:
                    g_explain_game = None
                    self._state = States.LEAVING_PERSON

            # --------------------------------------------------------------------------------------------
            elif States.GAME_PART_1 == self._state:

                if g_game_part_1 is None:
                    g_game_part_1 = self.game_part_1()

                try:
                    if not g_game_part_1.next():
                        g_game_part_1 = None
                        self._state = States.PREPARE_FOR_PART_2
                except StopIteration:
                    assert (False)
                    g_game_part_1 = None
                    self._state = States.SPENDING_TIME

                # user detected left spot
                hpa = has_person_left()
                if hpa is True:
                    g_game_part_1 = None
                    self._state = States.LEAVING_PERSON

            # --------------------------------------------------------------------------------------------
            elif States.PREPARE_FOR_PART_2 == self._state:

                if g_prepare_part_2 is None:
                    g_prepare_part_2 = self.prepare_part_2()

                try:
                    if not g_prepare_part_2.next():
                        g_prepare_part_2 = None
                        self._state = States.GAME_PART_2
                except StopIteration:
                    assert (False)
                    g_prepare_part_2 = None
                    self._state = States.SPENDING_TIME

                # user detected left spot
                hpa = has_person_left()
                if hpa is True:
                    g_prepare_part_2 = None
                    self._state = States.LEAVING_PERSON

            # --------------------------------------------------------------------------------------------
            elif States.GAME_PART_2 == self._state:
                if g_game_part_2 is None:
                    g_game_part_2 = self.game_part_2()

                try:
                    if not g_game_part_2.next():
                        g_game_part_2 = None
                        self._state = States.SAY_GOOD_BYE
                except StopIteration:
                    assert (False)
                    g_game_part_2 = None
                    self._state = States.SPENDING_TIME

                # user detected left spot
                hpa = has_person_left()
                if hpa is True:
                    g_game_part_2 = None
                    self._state = States.LEAVING_PERSON

            # --------------------------------------------------------------------------------------------
            elif States.SAY_GOOD_BYE == self._state:
                if g_say_good_bye is None:
                    g_say_good_bye = self.say_good_bye()

                try:
                    if not g_say_good_bye.next():
                        g_say_good_bye = None
                        self._state = States.SPENDING_TIME
                except StopIteration:
                    assert (False)
                    g_say_good_bye = None
                    self._state = States.SPENDING_TIME

            # --------------------------------------------------------------------------------------------
            elif States.GOOD_NIGHT == self._state:
                if g_good_night is None:
                    g_good_night = self.good_morning()

                try:
                    if not g_good_night.next():
                        shared_data.done = True
                        break
                except StopIteration:
                    g_good_night = None

            # --------------------------------------------------------------------------------------------
            time.sleep(0.5)

        print('leaving baxter routine')

    def good_morning(self):
        '''
        -Connect kinect server
        -Enable Baxter
        -Untuck arms

        :return:
        '''

        act = copy.deepcopy(behaviors.wait_in_natural)
        self.set_action(act)
        while not shared_data.done:
            if act.get_remaining_time() < 0:
                yield False
                break
            yield True

        yield False  # Go next

    def good_bye(self):
        '''
        -Tuck Baxters Arms
        -Disable Baxter
        -Disconnect kinect server

        :return:
        '''

        """
        Exits example cleanly by moving head to neutral position and
        maintaining start state
        """

        yield False

    def spend_time(self):
        act = None
        if len(self._kinect.get_kinect_data().people) == 0:
            act = behaviors.get_sleeping()
            self.sleeping = True
        else:
            act = random.choice(self._spending_time_actions)
            self.sleeping = False
        self.set_action(act)
        while not shared_data.done:
            if self.sleeping and \
                    (len(self._kinect.get_kinect_data().people) > 0):
                # Wakeup here
                yield False
            elif act.get_remaining_time() < 0:
                yield False
            else:
                yield True

    def leaving_person(self):
        # reaction = random.randint(1, len(behaviors.leaving_person_actions)) - 1

        act = behaviors.get_leaving()
        ''':type :Action'''
        self.set_action(act)
        while not shared_data.done:
            if act.get_remaining_time() < 0:
                yield False
                break
            yield True

    def explain_game(self):
        '''
        -Tell them you will play a game
        -Explain game 1
        -Explain game 2
        -Ask for being ready
        :return:
        '''

        act = behaviors.get_greeting()
        ''':type :Action'''
        self.set_action(act)
        while not shared_data.done:
            if act.get_remaining_time() < 0:
                yield False
                break
            yield True

    def game_part_1(self):
        '''
        -Do same actions as person
        -For forbidden actions change facial expression (give - point)
        -Continue for 1 minute
        :return:
        '''

        act = behaviors.get_part_1()
        ''':type :Action'''
        self.set_action(act)
        while not shared_data.done:
            if act.get_remaining_time() < 0:
                yield False
                break
            yield True

    def prepare_part_2(self):
        '''
        -Show score
        -Ask them to be ready
        :return:
        '''
        act = behaviors.get_ready_part_2()
        ''':type :Action'''
        self.set_action(act)
        while not shared_data.done:
            if act.get_remaining_time() < 0:
                yield False
                break
            yield True

    def game_part_2(self):
        '''
        -Choose 10 pose from 20 in random

        :return:
        '''
        act = behaviors.get_part_2()
        ''':type :Action'''
        self.set_action(act)
        while not shared_data.done:
            if act.get_remaining_time() < 0:
                yield False
                break
            yield True

    def say_good_bye(self):
        '''
        :return:
        '''
        act = behaviors.get_say_goodbye_good()
        ''':type :Action'''
        self.set_action(act)
        while not shared_data.done:
            if act.get_remaining_time() < 0:
                yield False
                break
            yield True
