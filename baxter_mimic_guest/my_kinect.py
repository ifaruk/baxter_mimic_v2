import pickle
import threading
from types import *

import basic_data as bd
import conf
from basic_data import Point
import shared_data
from host_con import Host_con


class Kinect_data:
    """
    :type pos_ja:dict[str,float]
    :type pos_jc:dict[str,float]
    :type people:list[Point]    
    """

    def __init__(self, pos_joint_angles, pos_joint_coordinates, people_coord):
        self.pos_ja = pos_joint_angles
        self.pos_jc = pos_joint_coordinates
        self.people = people_coord


class My_kinect(object):
    '''
    :type _special_visitor:Point
    
    
    '''

    def __init__(self, host):
        assert (isinstance(host, Host_con))
        self._host = host

        self._kinect_listener_routine = None
        self._kinect_data_lock = threading.Lock()
        self._kinect_data = Kinect_data(None, None, [])  # dict of joint_names-> joint_angles:float
        self._prev_kinect_data = Kinect_data(None, None, [])  # dict of joint_names-> joint_angles:float

        self._done = False
        self._prev_special_visitor = (None, 0)
        pass

    def __enter__(self):
        # Kinect listener Initialization
        # self._kinect_listener_routine = threading.Thread(target=self.dummy_kinect_listener, name="Kinect_listener")
        self._kinect_listener_routine = threading.Thread(target=self.kinect_listener, name="Kinect_listener")
        self._kinect_listener_routine.start()

        shared_data.ready_my_kinect = True
        print("My_kinect ready!")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        shared_data.done = True
        print("My_kinect terminating!")
        if self._kinect_listener_routine is not None and self._kinect_listener_routine.is_alive():
            self._kinect_listener_routine.join()
        shared_data.done_my_kinect = True
        print("My_kinect terminated!")
        if exc_val is not None:
            raise exc_val

    # def get_kinect_x_limit(self, distance):
    #     assert (isinstance(distance, (float, int)))
    #     x = float(distance * math.tan(conf.kinect_FOV / 2))
    #     return (-x, x)

    def set_kinect_data(self, kinect_data, block=True):
        # assert(isinstance(kinect_data,Kinect_data))
        # self._kinect_data = kinect_data
        # return
        if self._kinect_data_lock.acquire(block):
            try:
                self._kinect_data = kinect_data
                self._prev_kinect_data = self._kinect_data
            finally:
                self._kinect_data_lock.release()

    def get_kinect_data(self, block=True):
        '''
        :rtype:Kinect_data
        '''
        # return self._kinect_data
        #
        if self._kinect_data_lock.acquire(block):
            try:
                kinect_data = self._kinect_data
            finally:
                self._kinect_data_lock.release()
            return kinect_data
        else:
            return self._prev_kinect_data

    def kinect_listener(self):
        self._special_visitor = None
        self._special_visitor_state = 0
        self._special_visitor_state_size = 10
        self._special_visitor_lock = threading.Lock()

        # def calculate_average(list_of_dict):
        #     '''
        #     :type list_of_dict:list[dict[str,float]]
        #     :param list_of_dict:
        #     :return:
        #     '''
        #     if list_of_dict is None:
        #         return None
        #
        #     assert (isinstance(list_of_dict, list))
        #     if len(list_of_dict) > 0:
        #         assert (isinstance(list_of_dict[0], tuple))
        #         assert (isinstance(list_of_dict[0][0], dict))
        #         assert (isinstance(list_of_dict[0][1], dict))
        #     else:
        #         return None
        #
        #     result = (dict(),dict())
        #     for side in range(2):
        #         for key in list_of_dict[side][0].keys():
        #             result[side][key] = sum(member[key] for member in list_of_dict[side][]) / len(list_of_dict)
        #     return result

        retry_connection = 10
        while not shared_data.done:
            kinect_con = self._host.open_connection(conf.data_port, 60)
            self.kl_pos_joint_angle_list = []
            if kinect_con is not None:
                while not shared_data.done:
                    data = None
                    try:
                        f = kinect_con.makefile('rb', 300)
                        data = pickle.load(f)
                        """:type :((dict[str,float],dict[str,(float,float,float)]) , list[(float,float,float)])"""
                    except IndexError as e:
                        pass
                    except Exception as e:
                        data = None
                    finally:
                        f.close()

                    if data is not None:
                        assert (isinstance(data, tuple))
                        assert (isinstance(data[0], (NoneType, tuple)))
                        assert (isinstance(data[1], (NoneType, dict)))
                        assert (isinstance(data[2], list))
                        '''
                        location of people moving around. This a persons location can be lost or order can be changed
                        '''
                        for d in data[2]:
                            d.color = (0, 0, 0)
                        angles = None
                        if data[0] is not None:
                            angles = bd.get_joint_angle('left', **{key.split('_')[1]: val for key, val in data[0][0].items()}), bd.get_joint_angle('right', **{
                            key.split('_')[1]: val for key, val in data[0][1].items()})
                        self.set_kinect_data(Kinect_data(angles,
                                                         data[1],
                                                         data[2]),
                                             True)
                    else:
                        self.set_kinect_data(Kinect_data(None, None, []))

            elif self._host.is_OK():
                retry_connection -= 1
            elif not self._host.is_OK() or retry_connection < 0:
                raise Exception("Could not connected to kinect server.")

                # def set_special_visitor_event(self, blocking):
                #     if self._special_visitor_lock.acquire(blocking):
                #         try:
                #             self._special_visitor_state += 1
                #         finally:
                #             self._special_visitor_lock.release()
                #
                # def set_special_visitor(self, visitor, blocking):
                #     '''
                #     :param blocking:
                #     :type visitor:Point|None
                #     '''
                #     if self._special_visitor_lock.acquire(blocking):
                #         try:
                #             if visitor is None:
                #                 self._special_visitor_state = 0
                #             self._special_visitor = visitor
                #         finally:
                #             self._special_visitor_lock.release()
                #
                # def get_special_visitor(self, blocking):
                #     sv = None
                #     svs = 0
                #     if self._special_visitor_lock.acquire(blocking):
                #         try:
                #             sv = self._special_visitor
                #             svs = self._special_visitor_state
                #         finally:
                #             self._special_visitor_lock.release()
                #     return sv, svs

                # def dummy_kinect_listener(self):
                #     self._special_visitor = None
                #     self._special_visitor_state = 0
                #     self._special_visitor_state_size = 10
                #     self._special_visitor_lock = threading.Lock()
                #
                #     sva = [(bd.get_joint_angle('left', -0.418, 0.129, 0.378, 0.538, 0.477, 0.089, 0.160),
                #             bd.get_joint_angle('right', 0.279, 0.976, 1.020, 1.172, -2.894, 1.352, 0.091)),
                #            (bd.get_joint_angle('left', 0.335, 0.088, -0.190, 0.717, -0.454, 1.011, -1.569),
                #             bd.get_joint_angle('right', -0.301, 0.013, -2.838, 2.300, -2.040, 0.106, -0.016)),
                #            (bd.get_joint_angle('left', 0.699, -0.403, -2.175, 0.978, 0.921, 0.461, -0.879),
                #             bd.get_joint_angle('right', -0.611, -0.191, 0.982, 1.263, 0.170, 1.072, 0.572)),
                #            (bd.get_joint_angle('left', -0.229, -0.416, -1.276, 1.295, 1.284, -0.214, -1.391),
                #             bd.get_joint_angle('right', 0.248, -0.484, 1.181, 1.178, -0.065, 0.479, 1.468)),
                #            (bd.get_joint_angle('left', -0.10, -0.416, -1.276, 1.295, 1.284, -0.214, -1.391),  # hands are hitting
                #             bd.get_joint_angle('right', 0.10, -0.484, 1.181, 1.178, -0.065, 0.479, 1.468)),
                #            (bd.get_joint_angle('left', 0, 0, 0, 0, 0, 0, 0),
                #             bd.get_joint_angle('right', 0, 0, 0, 0, 0, 0, 0))
                #            ]
                #
                #     def get_new_dummy_person():
                #
                #         color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
                #
                #         x = 0
                #         z = 0
                #         if random.randint(0, 99) >= 50:
                #             # On Vertical Edge
                #             x = random.choice([conf.kinect_world_x_min - conf.edge_constant, conf.kinect_world_x_max + conf.edge_constant])
                #             z = numpy.random.uniform(conf.kinect_dummy_person_z_limit - conf.edge_constant,
                #                                      conf.kinect_world_z_max + conf.edge_constant)
                #         else:
                #             # On Horizontal Edge
                #             x = numpy.random.uniform(conf.kinect_world_x_min - conf.edge_constant,
                #                                      conf.kinect_world_x_max + conf.edge_constant)
                #             z = random.choice([conf.kinect_dummy_person_z_limit - conf.edge_constant, conf.kinect_world_z_max + conf.edge_constant])
                #
                #         p = Point(x, 1.7, z)
                #         main_direction = random.uniform(-math.pi, math.pi)
                #         main_speed = random.uniform(0.2, 0.5) / 5
                #         while not shared_data.done:
                #             cur_speed = abs(numpy.random.normal(main_speed, main_speed / 3))
                #             cur_direction = numpy.random.normal(main_direction, math.pi / 10)
                #
                #             p = Point(p.x + cur_speed * math.cos(cur_direction),
                #                       p.y,
                #                       p.z + cur_speed * math.sin(cur_direction))
                #             p.color = color
                #
                #             if p.z <= conf.kinect_dummy_person_z_limit:
                #                 p.z = conf.kinect_dummy_person_z_limit
                #                 main_direction = random.uniform(0, math.pi)
                #
                #             # calculate visible x
                #             v_x_min, v_x_max = self.get_kinect_x_limit(p.z)
                #
                #             # If outside of edges
                #             if not (conf.kinect_world_x_min - conf.edge_constant <= p.x <= conf.kinect_world_x_max + conf.edge_constant
                #                     and conf.kinect_world_z_min - conf.edge_constant <= p.z <= conf.kinect_world_z_max + conf.edge_constant):
                #                 break
                #             # if outside of visible area
                #             elif not (v_x_min < p.x < v_x_max):
                #                 yield None
                #             else:
                #                 yield p
                #
                #     people_generator = []
                #     ''':type :list[dummpy_person()]'''
                #     while not shared_data.done:
                #
                #         while len(people_generator) < 10:
                #             people_generator.append(get_new_dummy_person())
                #
                #         people = []
                #         remaining_people = []
                #         for p in people_generator:
                #             try:
                #                 pn = p.next()
                #                 if pn is not None:
                #                     people.append(pn)
                #                 remaining_people.append(p)
                #             except StopIteration:
                #                 pass
                #         people_generator = remaining_people
                #
                #         pos_joint_angles = None
                #         pos_joint_coordinates = None
                #
                #         sv_point, sv_state = self.get_special_visitor(False)
                #         if sv_point is None:
                #             sv_point, sv_state = self._prev_special_visitor
                #         else:
                #             self._prev_special_visitor = (sv_point, sv_state)
                #
                #         if sv_point is not None:
                #             people.append(sv_point)
                #             # if person is on focus point
                #
                #             z_min = conf.person_of_interest_z - conf.person_of_interest_box_depth_z
                #             z_max = conf.person_of_interest_z + conf.person_of_interest_box_depth_z
                #
                #             x_min = conf.person_of_interest_x - conf.person_of_interest_box_width_x
                #             x_max = conf.person_of_interest_x + conf.person_of_interest_box_width_x
                #
                #             if z_min <= sv_point.z <= z_max \
                #                     and x_min <= sv_point.x <= x_max:
                #                 pos_joint_angles = sva[sv_state % len(sva)]
                #
                #         self.set_kinect_data(Kinect_data(pos_joint_angles, pos_joint_coordinates, people))
                #
                #         time.sleep(0.1)
