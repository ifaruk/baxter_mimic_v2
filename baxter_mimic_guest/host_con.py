#!/usr/bin/env python

__author__ = 'ifyalciner@gmail.com'

import socket
import threading
import time

import conf
import shared_data


class Host_con(object):
    def __init__(self):
        # --------------------------------------------
        self._heartbeat_listener = None
        self._host_heartbeat_OK = False

        self._socket_lock = threading.Lock()
        self._sockets = []

    def __enter__(self):
        self._heartbeat_listener = threading.Thread(target=self.heartbeat, name="Host_con")
        self._heartbeat_listener.start()

        shared_data.ready_host_con = True
        print("Host_con ready!")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        shared_data.done = True
        print("Host_con terminating!")
        self._socket_lock.acquire(True)
        for s in self._sockets:
            ''':type : socket.socket'''
            s.close()
        if self._heartbeat_listener is not None and self._heartbeat_listener.is_alive():
            self._heartbeat_listener.join()
        shared_data.done_host_con = True
        print("Host_con terminated!")
        if exc_val is not None:
            raise exc_val

    def heartbeat(self):
        self._host_heartbeat_OK = True
        heartbeat_socket = None
        heartbeat_output_prev_time=time.time()
        try:
            # Connect Server
            print("Connecting heartbeat service.")
            heartbeat_socket = self.open_connection(conf.heartbeat_port, 120)
            if heartbeat_socket is not None:
                print("Connected to heartbeat service.")
                heartbeat_socket.settimeout(10)

                # Send and Receive Heartbeat
                while not shared_data.done:
                    response = heartbeat_socket.recv(16)
                    if response[:2] == b'bu':
                        # print("HB received.")
                        heartbeat_socket.send(b'mp')
                        self._host_heartbeat_OK = True
                        if time.time() - heartbeat_output_prev_time > 5: #print every 5 seconds
                            print("Heartbeat OK")
                            heartbeat_output_prev_time = time.time()
                    else:
                        raise Exception("Heartbeat service communication corrupted!")
                        # self._host_heartbeat_OK = False
                        # break
            else:
                raise Exception("Could not connect to heartbeat service!")

        except Exception as e:
            print("Connection terminated by host!")

        finally:
            shared_data.done = True
        print("Host heartbeat lost!")

    def open_connection(self, port, timeout):
        ret_soc = None
        connect_try_timeout = timeout
        prev_time = time.time()
        while not shared_data.done:
            try:
                # Create a TCP/IP socket
                ret_soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                ret_soc.settimeout(10)
                # Timeout set to 10 seconds
                print("connecting " + str(conf.server_hostname) + ":" + str(port))
                ret_soc.connect((str(conf.server_hostname), port))  # heartbeat port is 10000
                print("connected to " + str(conf.server_hostname) + ":" + str(port))

                try:
                    self._socket_lock.acquire(True)
                    self._sockets.append(ret_soc)
                finally:
                    if self._socket_lock.locked():
                        self._socket_lock.release()

                break
            except (Exception, socket.timeout) as e:
                print(e)
                time.sleep(1)
                connect_try_timeout -= time.time() - prev_time
                prev_time = time.time()
                ret_soc = False
                if connect_try_timeout < 0:
                    raise Exception("Could not connected to host!")
        return ret_soc

    def is_OK(self):
        return self._host_heartbeat_OK
