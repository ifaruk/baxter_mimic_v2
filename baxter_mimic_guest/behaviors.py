import random

import conf
from action import arm, head, speech
from action.actions import Action_list_member, Action, Focus_point, Point


# ------------------------------------------------------------------------------------------------------------------------

def get_complete_action(focus_point, arm_action_duration, period, s, h, a, ):
    '''
    :param focus_point: 
    :param arm_action_duration: arm action duration for head and  
    :param period: how long this action will take
    :param s: speech action
    :param h: head action
    :param a: arm action
    :return: 
    '''
    speech_action = speech.Speak(file=s)
    if speech_action.duration > period:
        period = speech_action.duration

    return Action(speech=[Action_list_member(speech_action, 0, speech_action.duration)],
                  head=[Action_list_member(h(focus_point), 0, speech_action.duration),
                        Action_list_member(head.Neutral(focus_point), speech_action.duration, period)],
                  arm=[Action_list_member(a(focus_point), 0, arm_action_duration),
                       Action_list_member(arm.Neutral(focus_point), arm_action_duration, period)])


def get_spending_time_random_actions(action_duration, duration):
    # if focus_point is None:
    #     if kinect_data is not None and kinect_data.people is not None and len(kinect_data.people) > 0:
    #         p = random.choice(kinect_data.people)
    #         focus_point = Focus_point(p, Focus_point.BEHAVIOR_FOLLOW_CLOSEST)
    #     if focus_point is None:
    #         # select a point
    #         focus_point = conf.poi_focus

    focus = Focus_point(Point(0,
                              1.7,
                              conf.person_of_interest_z),
                        Focus_point.BEHAVIOR_FOLLOW_CLOSEST)

    action_list = []
    action_list += [get_complete_action(focus, action_duration, duration, s, h, a) \
                    for s in speech.spending_time \
                    for h in [head.Happy, head.Talking] \
                    for a in [arm.Wave, arm.Pointing, arm.Neutral, arm.Two_hand_up_waving]]

    return action_list


wait_in_natural = get_complete_action(conf.poi_focus, 5, 5, speech.speak_silence, head.Neutral, arm.Neutral)


# ------------------------------------------------------------------------------------------------------------------------
# GREETING AND EXPLAINING THE GAME

def get_sleeping():
    period = 9000
    focus = conf.poi_focus
    speech_action = speech.Speak(file=speech.speak_se_yawn_0)
    if speech_action.duration > period:
        period = speech_action.duration
    return Action(speech=[Action_list_member(speech_action, 0, speech_action.duration)],
                  head=[Action_list_member(head.Sleep(focus), 0, period)],
                  arm=[Action_list_member(arm.About_to_sleep(focus), 0, period)])


def get_greeting():
    def get_talking_action(file, focus_point):
        sa = speech.Speak(file)
        sa_silence = speech.Speak(speech.speak_silence)
        return Action(speech=[Action_list_member(sa,
                                                 0,
                                                 sa.duration),
                              Action_list_member(sa_silence,
                                                 sa.duration,
                                                 sa.duration + 0.25)],
                      head=[Action_list_member(head.Talking(focus_point),
                                               0,
                                               sa.duration),
                            Action_list_member(head.Talking_wait(focus_point),
                                               sa.duration,
                                               sa.duration + 0.25)],
                      arm=[Action_list_member(arm.Do(arm.do_natural_left_angles,
                                                     arm.do_natural_right_angles),
                                              0,
                                              sa.duration + 0.25)])

    sp = random.choice(speech.intro)
    return get_talking_action(sp, conf.poi_focus)


def get_leaving():
    def get_talking_action(file, focus_point):
        sa = speech.Speak(file)
        sa_silence = speech.Speak(speech.speak_silence)
        return Action(speech=[Action_list_member(sa,
                                                 0,
                                                 sa.duration),
                              Action_list_member(sa_silence,
                                                 sa.duration,
                                                 sa.duration + 0.25)],
                      head=[Action_list_member(head.Talking(focus_point),
                                               0,
                                               sa.duration),
                            Action_list_member(head.Talking_wait(focus_point),
                                               sa.duration,
                                               sa.duration + 0.25)],
                      arm=[Action_list_member(arm.Wave(focus_point),
                                              0,
                                              sa.duration + 0.25)])

    sp = random.choice(speech.leave_early)
    return get_talking_action(sp, conf.poi_focus)


def get_part_1():
    ssp = 5  # single speak period
    # choice_list = [speech.speak_am_i_doing_fine_0,
    #                speech.speak_wow_0,
    #                speech.speak_how_is_this_0,
    #                speech.speak_is_this_good_0]
    choice_list = [speech.speak_silence]
    return Action(speech=[Action_list_member(speech.Speak(random.choice(choice_list)),
                                             i * ssp,
                                             (i + 1) * ssp) for i in range(0, int(conf.game_part_1_duration) / ssp)],
                  head=[Action_list_member(head.Happy(conf.poi_focus),
                                           0,
                                           conf.game_part_1_duration)],
                  arm=[Action_list_member(arm.Mimic_hard(conf.poi_focus),
                                          0,
                                          conf.game_part_1_duration)])


def get_ready_part_2():
    sa = speech.Speak(random.choice(speech.your_turn))
    return Action(speech=[Action_list_member(sa,
                                             0,
                                             sa.duration)],
                  head=[Action_list_member(head.Talking(conf.poi_focus),
                                           0,
                                           sa.duration)],
                  arm=[Action_list_member(arm.Pointing(conf.poi_focus),
                                          0,
                                          sa.duration)])


def get_part_2():
    from basic_data import get_symmetric
    choice_list = [speech.speak_silence]
    ssp = 3.0  # single action period.0
    # choice_list = [speech.speak_you_must_be_on_this_display_0,
    #                      speech.speak_where_did_you_learn_mimic_like_this_0,
    #                      speech.speak_we_have_a_tough_player_0,
    #                      speech.speak_you_can_do_better_0,
    #                      speech.speak_lets_see_what_you_can_do_0]

    # ------------------------------------
    lspeech = []
    lhead = []
    larm = []
    for i in range(0, int(conf.game_part_2_duration / ssp)):
        lspeech.append(Action_list_member(speech.Speak(random.choice(choice_list)),
                                          i * ssp,
                                          (i + 1) * ssp))
        # -----------------
        lhead.append(Action_list_member(head.Happy(conf.poi_focus),
                                        i * ssp,
                                        (i + 1) * ssp))
        # -----------------
        larm.append(Action_list_member(arm.Do(random.choice(arm.various_left_poses), get_symmetric(random.choice(arm.various_left_poses))),
                                       i * ssp,
                                       (i + 1) * ssp))

    return Action(lspeech, lhead, larm)


def get_say_goodbye_good():
    sa = speech.Speak(random.choice(speech.good_game_result))
    return Action(speech=[Action_list_member(sa,
                                             0,
                                             sa.duration)],
                  head=[Action_list_member(head.Talking(conf.poi_focus),
                                           0,
                                           sa.duration)],
                  arm=[Action_list_member(arm.Wave(conf.poi_focus),
                                          0,
                                          sa.duration)])


def get_say_goodbye_bad():
    sa = speech.Speak(random.choice(speech.bad_game_result))
    return Action(speech=[Action_list_member(sa,
                                             0,
                                             sa.duration)],
                  head=[Action_list_member(head.Talking(conf.poi_focus),
                                           0,
                                           sa.duration)],
                  arm=[Action_list_member(arm.Wave(conf.poi_focus),
                                          0,
                                          sa.duration)])


def get_random():
    return Action(speech=[Action_list_member(speech.Speak(speech.speak_silence),
                                             0.0,
                                             10.0)],
                  head=[Action_list_member(head.Happy(conf.poi_focus),
                                           0.0,
                                           10.0)],
                  arm=[Action_list_member(arm.Random(conf.poi_focus),
                                          0,
                                          10.0)])
