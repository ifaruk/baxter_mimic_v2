import math
import socket
import threading
import time

import baxter_interface
import cv2
import cv_bridge
import numpy
import numpy as np
import rosgraph
# ros & baxter packages
import rospy
from sensor_msgs.msg import (Image, )

import basic_data as bd
# custom packages
import conf
import shared_data
from tools import closestDistanceBetweenLines
from tuck_arms import Tuck


class Baxter_data(object):
    def __init__(self):
        self.head_angle=None
        self.left_ja=None
        self.right_ja=None


class My_baxter(object):
    # ----------------------------------------------
    def __init__(self):
        self._ros_init = False
        # ----------------------------------------------------------------------------------

        self._baxter_connected = False
        self._baxter_ready = False

        self._head = None
        self._limb = {'right': None,
                      'left' : None}

        self._prev_angles = {'right': None,
                             'left' : None}

        self._baxter_data_lock = threading.Lock()
        self._baxter_data = Baxter_data()  # dict of joint_names-> joint_angles:float

        # ---------------------------------------------
        self._head_last_face_file = None

        self._endpoint_prev_time = None
        self._lendpoint_prev_loc = None
        self._rendpoint_prev_loc = None

        self._prev_prox = {'left', 0.0,
                           'right', 0.0}

        self._prev_time=0.0
        self._prev_left = None
        self._prev_right = None

        self._tf_listener = None

    def __enter__(self):

        while not shared_data.done:
            try:
                rosgraph.Master('/rostopic').getPid()
                break
            except socket.error:
                print("Waiting for rosmaster...")
                time.sleep(2)

        # Rospy  and baxter initialization
        self._ros_init = False
        while not shared_data.done:
            try:
                print("Initializing ROS Node")
                rospy.init_node('baxter_teleoperation')
                rospy.on_shutdown(self.clean_shutdown)
                self._ros_init = True
                break
            except Exception as e:
                print(e)
                raise e

        # self._tf_listener = tf.TransformListener()

        shared_data.ready_my_ros = True
        print("My_ros ready!")

        # ---------------------------------------------------------------------------------
        # Baxter Init

        # verify robot is enabled
        while not shared_data.done:
            try:
                print("Getting robot state... ")
                self._robot_state = baxter_interface.RobotEnable(baxter_interface.CHECK_VERSION)
                break
            except OSError as e:
                print(e)
                time.sleep(1)
            except Exception as e:
                print(e)

        try:
            print("Check if robot is enabled... ")
            self._init_state = self._robot_state.state().enabled
            self._baxter_connected = False
            if not self._init_state:
                print("Enabling robot... ")
                self._robot_state.enable()
                print("Running. Ctrl-c to quit")
            self._baxter_ready = True

            tuck = Tuck(False)
            tuck.supervised_tuck()

            self._head = baxter_interface.Head()
            self._limb['left'] = baxter_interface.Limb('left')
            self._limb['right'] = baxter_interface.Limb('right')


            # self._limb['left'].set_joint_position_speed(0.7)
            # self._limb['right'].set_joint_position_speed(0.7)
            #
            # self._limb['left'].set_command_timeout(0.23)
            # self._limb['right'].set_command_timeout(0.23)

        except Exception as e:
            print(e)
            self._done = True

        self._prev_angles['right'] = self._limb['right'].joint_angles()
        self._prev_angles['left'] = self._limb['left'].joint_angles()

        shared_data.ready_my_baxter = True
        print("My_baxter ready!")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):

        shared_data.done = True
        print("My_ros terminating!")
        rospy.logwarn('Aborting: Shutting down safely...')
        shared_data.done_my_ros = True

        print("My_baxter terminating!")
        self.clean_shutdown()
        shared_data.done_my_baxter = True
        print("My_baxter terminated!")
        if exc_val is not None:
            raise exc_val

    def clean_shutdown(self):
        shared_data.done = True
        if self._baxter_ready:
            tucker = Tuck(True)
            tucker.supervised_tuck()
            # self._reset_control_modes()
            if not self._init_state and self._robot_state.state().enabled:
                print("Disabling robot...")
                self._robot_state.disable()
            self._baxter_ready = False

    def set_baxter_data(self, baxter_data, block=True):
        assert(isinstance(baxter_data,Baxter_data))
        if self._baxter_data_lock.acquire(block):
            try:
                self._baxter_data = baxter_data
            finally:
                if self._baxter_data_lock.locked():
                    self._baxter_data_lock.release()

    def get_baxter_data(self, block=False):
        '''
        :rtype: Baxter_data
        :param block:
        :return:
        '''
        baxter_data = None
        if self._baxter_data_lock.acquire(block):
            try:
                baxter_data = self._baxter_data
            finally:
                self._baxter_data_lock.release()
        return baxter_data

    # def get_joint_locations(self):
    #     left = [None for i in range(4)]
    #     right = [None for i in range(4)]
    #
    #     try:
    #         left[0], rot = self._tf_listener.lookupTransform('/base', '/left_lower_shoulder', rospy.Time(0))
    #         left[1], rot = self._tf_listener.lookupTransform('/base', '/left_lower_elbow', rospy.Time(0))
    #         left[2], rot = self._tf_listener.lookupTransform('/base', '/left_lower_forearm', rospy.Time(0))
    #         left[3], rot = self._tf_listener.lookupTransform('/base', '/left_gripper_base', rospy.Time(0))
    #
    #         right[0], rot = self._tf_listener.lookupTransform('/base', '/right_lower_shoulder', rospy.Time(0))
    #         right[1], rot = self._tf_listener.lookupTransform('/base', '/right_lower_elbow', rospy.Time(0))
    #         right[2], rot = self._tf_listener.lookupTransform('/base', '/right_lower_forearm', rospy.Time(0))
    #         right[3], rot = self._tf_listener.lookupTransform('/base', '/right_gripper_base', rospy.Time(0))
    #     except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
    #         left = [None for i in range(4)]
    #         right = [None for i in range(4)]
    #
    #     return left, right

    def get_proximity(self):
        arm_radius = 0.2  # meters
        torso_radius = 0.4

        left, right = self.get_joint_locations()
        if left[0] is None:
            return None, None

        '''
        Facing baxter   :+X is through Observer leaving Baxter
                        :+Y is through Right hand side of Observer
                        :+Z is through Upward
        '''

        l_line_segments = [(np.array(left[i]), np.array(left[i + 1])) for i in range(len(left))]
        r_line_segments = [(np.array(left[i]), np.array(left[i + 1])) for i in range(len(left))]
        torso_line_segment = [(np.array([0.0, 0.0, -2.0]), np.array([0.0, 0.0, +2.0]))]

        # Find left arm proximity
        l_to_r = [closestDistanceBetweenLines(l[0], l[1], o[0], o[1], True) for l in l_line_segments for o in r_line_segments]
        l_to_t = [closestDistanceBetweenLines(l[0], l[1], o[0], o[1], True) for l in l_line_segments for o in torso_line_segment]
        r_to_l = [closestDistanceBetweenLines(r[0], r[1], o[0], o[1], True) for r in r_line_segments for o in l_line_segments]
        r_to_t = [closestDistanceBetweenLines(r[0], r[1], o[0], o[1], True) for r in r_line_segments for o in torso_line_segment]

        l_closest = min([dist - arm_radius for p0, p1, dist, in l_to_r] + [dist - torso_radius for p0, p1, dist in l_to_t])
        r_closest = min([dist - arm_radius for p0, p1, dist, in r_to_l] + [dist - torso_radius for p0, p1, dist in r_to_t])

        return l_closest, r_closest

    def get_speed(self):
        # lprox, r_prox = self.get_proximity()
        # return (lspeed, rspeed)
        pass

    def update_arm_speed(self, left, right):
        '''
        
        :param left: dict[str,float]
        :param right: dict[str,float] 
        :param baxter_data: 
        :param kinect_data: 
        :return: 
        '''


        # if time.time()-self._prev_time < 0.03:
        #     return

        self._prev_time = time.time()


        # if left is not None:
        #     self._limb['left'].set_joint_positions(left)
        #     self._prev_angles['left'] = left
        #
        # if right is not None:
        #     self._limb['right'].set_joint_positions(right)
        #     self._prev_angles['right'] = right
        # return

        def get_angular_speed(current_angle, desired_angle, range, speed_limit):
            return_val = 0.0
            if math.fabs(desired_angle - current_angle) > conf.arm_action_single_angle_threshold:
                return_val = ((desired_angle - current_angle) / range) * conf.arm_angular_speed_coeff
            else:
                return 0.0
            return speed_limit * return_val
            # ------------------------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------------------------
        def process_arm(side, new_angles, safety_speed_coef=1.0):
            if new_angles is None:
                new_angles = self._prev_angles[side]
            else:
                self._prev_angles[side] = new_angles

            cur_angles = self._limb[side].joint_angles()

            jvals = [(side + '_' + joint_name, bd.joint_ranges[joint_name], bd.joint_speed_limits[joint_name]) for joint_name in bd.joint_names]
            jspeeds = [get_angular_speed(cur_angles[jn],
                                         new_angles[jn],
                                         jr,
                                         js * safety_speed_coef) for jn, jr, js in jvals]

            # Stop all joints
            if False not in (numpy.array([abs(a) for a in jspeeds]) < conf.arm_action_all_angle_threshold):
                jspeeds = [0.0 for i in jspeeds]
                # self._limb[side].set_

            jsend = dict(zip([name for name, jr, js in jvals], jspeeds))
            # print(side+':'+str(jspeeds))
            self._limb[side].set_joint_velocities(jsend)

        if not self._baxter_ready:
            return

        lspeed, rspeed = (1.0, 1.0)  # get_speed_reduction()
        process_arm('left', left, lspeed)
        process_arm('right', right, rspeed)
        # print(str(left) + ':' + str(right))

    def update_head(self, angle, speed, face_file):
        def send_image(path):
            """
            Send the image located at the specified path to the head
            display on Baxter.

            @param path: path to the image file to load and send
            """
            img = cv2.imread(path)
            msg = cv_bridge.CvBridge().cv2_to_imgmsg(img, encoding="bgr8")
            pub = rospy.Publisher('/robot/xdisplay', Image, latch=True, queue_size=1)
            pub.publish(msg)
            # Sleep to allow for image to be published.
            # time.sleep(1)

        if not self._baxter_ready:
            return

        if angle is not None:
            try:
                self._head.set_pan(angle, speed=speed, timeout=0.1)
            except OSError as e:
                if e.errno != 110:
                    raise e

        if face_file is not None and face_file != self._head_last_face_file:
            self._head_last_face_file = face_file
            send_image(face_file)

    def is_OK(self):
        return self._baxter_ready

        # def _reset_control_modes(self):
        #     rate = rospy.Rate(self._rate)
        #     for _ in xrange(100):
        #         if rospy.is_shutdown():
        #             return False
        #         self._left_limb.exit_control_mode()
        #         self._right_limb.exit_control_mode()
        #         self._pub_rate.publish(100)  # 100Hz default joint state rate
        #         rate.sleep()
        #     return True
