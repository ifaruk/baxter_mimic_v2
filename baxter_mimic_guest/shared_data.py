from signal import signal
import threading

ready_baxter_routine = False
ready_host_con = False
ready_my_baxter = False
ready_my_kinect = False
ready_my_kinect_display = False
ready_my_ros = False

done = False
done_baxter_routine = False
done_host_con = False
done_my_baxter = False
done_my_kinect = False
done_my_kinect_display = False
done_my_ros = False

_terminate_lock = threading.RLock()
terminate_condition = threading.Condition(_terminate_lock)
