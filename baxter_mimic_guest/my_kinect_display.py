# import pygame
# import threading
# from tools import translate
#
# from basic_data import Point
# from my_kinect import My_kinect
# import conf
# import shared_data
#
#
# class My_kinect_display(object):
#     '''
#     :type _kinect:My_kinect
#     '''
#
#     def __init__(self, kinect):
#         assert (isinstance(kinect, My_kinect))
#
#         self._kinect = kinect
#
#         self._done = False
#         # ----------------------------------------------------------------------------------
#
#         self._draw_routine = None
#         pass
#
#     def __enter__(self):
#
#         # --------------------------------------------
#         pygame.init()
#         self._draw_people_screen = pygame.display.set_mode([conf.draw_people_disp_x, conf.draw_people_disp_y])
#         self._draw_engage_point_speech = (None, None, None)
#         self._draw_engage_point_head = (None, None, None)
#         self._draw_engage_point_arm = (None, None, None)
#         self._draw_data_lock = threading.Lock()
#         pygame.display.set_caption("People moving around.")
#
#         # -----------------------------------------------------------------------------------
#         self._draw_routine = threading.Thread(target=self.simulation_routine, name="Kinect_display")
#
#         self._draw_routine.start()
#
#         shared_data.ready_my_kinect_display = True
#         print("My_kinect_display ready!")
#         return self
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         shared_data.done = True
#         print("My_kinect_display terminating!")
#         if self._draw_routine is not None and self._draw_routine.is_alive():
#             self._draw_routine.join()
#
#         pygame.quit()
#         shared_data.done_my_kinect_display = True
#         print("My_kinect_display terminated!")
#         if exc_val is not None:
#             raise exc_val
#
#     def set_draw_engage_point_speech(self, point):
#         self._draw_engage_point_speech = (point, (128, 0, 0), 'S')
#
#     def set_draw_engage_point_head(self, point):
#         self._draw_engage_point_head = (point, (0, 128, 0), 'H')
#
#     def set_draw_engage_point_arm(self, point):
#         self._draw_engage_point_arm = (point, (0, 0, 128), 'A')
#
#     def simulation_routine(self):
#
#         def kinect_world_to_disp(x, z):
#             x_ = int(
#                     translate(x, conf.kinect_world_x_max, conf.kinect_world_x_min, 0, conf.draw_people_disp_x))
#             y_ = int(
#                     translate(z, conf.kinect_world_z_max, conf.kinect_world_z_min, 0, conf.draw_people_disp_y))
#             return (x_, y_)
#
#         def disp_to_kinect_world(x, y):
#             x_ = translate(x, 0, conf.draw_people_disp_x,
#                            conf.kinect_world_x_max,
#                            conf.kinect_world_x_min)
#             z_ = translate(y, 0, conf.draw_people_disp_y,
#                            conf.kinect_world_z_max,
#                            conf.kinect_world_z_min)
#             return (x_, z_)
#
#         clock = pygame.time.Clock()
#         clock.tick(30)
#
#         while not shared_data.done:
#
#             for event in pygame.event.get():  # User did something
#
#                 on_display = pygame.mouse.get_focused()
#
#                 if event.type == pygame.QUIT:  # If user clicked close
#                     shared_data.done = True  # Flag that we are done so we exit this loop
#
#
#                 elif pygame.MOUSEBUTTONDOWN == event.type:
#                     self._kinect.set_special_visitor_event(True)
#
#                 elif pygame.MOUSEMOTION == event.type:
#                     if not on_display:
#                         self._kinect.set_special_visitor(None, False)
#                     if on_display:
#                         x, z = disp_to_kinect_world(*pygame.mouse.get_pos())
#                         z = max(2.0, z)
#                         p = Point(x, 1, z)
#                         p.color = (0, 0, 0)
#                         self._kinect.set_special_visitor(p, False)
#
#             baxter_x, baxter_y = kinect_world_to_disp(0, 0)
#
#             # Draw main screen
#             self._draw_people_screen.fill((255, 255, 255))
#
#             # Draw shades
#             s = pygame.Surface((conf.draw_people_disp_x, conf.draw_people_disp_y))  # the size of your rect
#             s.set_alpha(200)  # alpha level
#             s.fill((255, 255, 255))
#
#             (kx_min, kx_max) = self._kinect.get_kinect_x_limit(conf.kinect_world_z_max)
#
#             baxter_point = [baxter_x, baxter_y]
#             top_left = list(kinect_world_to_disp(kx_min, conf.kinect_world_z_max))
#             bottom_left = list(kinect_world_to_disp(kx_min, conf.kinect_world_z_min))
#             top_right = list(kinect_world_to_disp(kx_max, conf.kinect_world_z_max))
#             bottom_right = list(kinect_world_to_disp(kx_max, conf.kinect_world_z_min))
#
#             pygame.draw.polygon(s, (0, 0, 0), [baxter_point,
#                                                top_left,
#                                                bottom_left,
#                                                baxter_point,
#                                                top_right,
#                                                bottom_right,
#                                                baxter_point], 0)
#
#             al_tl_x, al_tl_y = kinect_world_to_disp(conf.person_of_interest_x + conf.person_of_interest_box_width_x,
#                                                     conf.person_of_interest_z + conf.person_of_interest_box_depth_z)
#             al_br_x, al_br_y = kinect_world_to_disp(conf.person_of_interest_x - conf.person_of_interest_box_width_x,
#                                                     conf.person_of_interest_z - conf.person_of_interest_box_depth_z)
#
#             pygame.draw.rect(s, (0, 100, 200), pygame.Rect(al_tl_x, al_tl_y, al_br_x - al_tl_x, al_br_y - al_tl_y), 2)
#             self._draw_people_screen.blit(s, [0, 0])
#
#             red_line_disp_y = int(
#                     translate(conf.kinect_person_z_min, conf.kinect_world_z_max, conf.kinect_world_z_min, 0,
#                               conf.draw_people_disp_y))
#             pygame.draw.line(self._draw_people_screen, (200, 20, 20), [0, red_line_disp_y],
#                              [conf.draw_people_disp_x, red_line_disp_y], 2)
#
#             myfont = pygame.font.SysFont("monospace", 30)
#             label = myfont.render('B', 1, (0, 0, 0))
#             self._draw_people_screen.blit(label, (baxter_x, baxter_y - 25))
#
#             kinect_data = self._kinect.get_kinect_data(True)
#             if kinect_data is not None and len(kinect_data.people) > 0:
#                 people = kinect_data.people
#                 for p in people:
#                     pygame.draw.circle(self._draw_people_screen, p.color, list(kinect_world_to_disp(p.x, p.z)), 10)
#
#                 for p, color, mark in [self._draw_engage_point_speech, self._draw_engage_point_head,
#                                        self._draw_engage_point_arm]:
#                     if p is None:
#                         continue
#                     coor_x, coor_y = kinect_world_to_disp(p.x, p.z)
#                     pygame.draw.line(self._draw_people_screen, color, [baxter_x, baxter_y], [coor_x, coor_y], 3)
#                     pygame.draw.circle(self._draw_people_screen, color, [coor_x, coor_y], 15, 1)
#                     myfont = pygame.font.SysFont("monospace", 10)
#                     label = myfont.render(mark, 1, color)
#                     self._draw_people_screen.blit(label, (coor_x, coor_y))
#
#             pygame.display.flip()
