import math


# host
server_hostname = None #Comes from The Host

heartbeat_port = None #Comes from The Host
data_port = None #Comes from The Host

# baxter
arm_angular_speed_coeff = None #Comes from The Host, 2.0
arm_action_all_angle_threshold = math.pi / 40  # if higher then this then there will be action
arm_action_single_angle_threshold = 0  # if higher then this then there will be action

# poi
engage_duration = None #Comes from The Host
disengage_duration = None #Comes from The Host

# kinect
person_of_interest_z = None #Comes from The Host
poi_focus = None

# Action
spare_time_action_period = None #Comes from The Host

#Game
game_part_1_duration = None #Comes from The Host
game_part_2_duration = None #Comes from The Host
