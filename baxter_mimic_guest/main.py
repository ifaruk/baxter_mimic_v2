import os
import signal
import sys
import time

import conf
import shared_data


def terminate_signal_handler(signal, frame):
    shared_data.done = True
    while not shared_data.done_my_ros or \
            not shared_data.done_my_kinect_display or \
            not shared_data.done_my_kinect or \
            not shared_data.done_my_baxter or \
            not shared_data.done_host_con or \
            not shared_data.done_baxter_routine:
        time.sleep(1)


def main():
    arguments = sys.argv[1:]
    assert (len(arguments) == 10)
    conf.engage_duration = float(arguments[0])
    conf.disengage_duration = float(arguments[1])
    conf.arm_angular_speed_coeff = float(arguments[2])
    conf.spare_time_action_period = float(arguments[3])
    conf.game_part_1_duration = float(arguments[4])
    conf.game_part_2_duration = float(arguments[5])
    conf.server_hostname = arguments[6]
    conf.person_of_interest_z = float(arguments[7])
    conf.heartbeat_port = int(arguments[8])
    conf.data_port = int(arguments[9])

    from basic_data import Focus_point, Point
    conf.poi_focus = Focus_point(Point(0.0,
                                       1.7,
                                       conf.person_of_interest_z),
                                 Focus_point.BEHAVIOR_FIND_CLOSEST)

    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)

    from my_baxter import My_baxter
    from my_kinect import My_kinect
    from baxter_routine import Baxter_routine
    from host_con import Host_con

    signal.signal(signal.SIGABRT, terminate_signal_handler)
    signal.signal(signal.SIGTSTP, terminate_signal_handler)
    signal.signal(signal.SIGQUIT, terminate_signal_handler)

    with Host_con() as h, \
            My_kinect(h) as mk, \
            My_baxter() as mbaxter, \
            Baxter_routine(mbaxter, mk) as mbb:
        while not shared_data.done:
            time.sleep(5)


if __name__ == '__main__':
    __main__ = "Baxter Master"
    main()

    # try:
    #     __main__ = "Baxter Master"
    #     main()
    # except:
    #     type, value, tb = sys.exc_info()
    #     traceback.print_exc()
    #     last_frame = lambda tb=tb: last_frame(tb.tb_next) if tb.tb_next else tb
    #     frame = last_frame().tb_frame
    #     ns = dict(frame.f_globals)
    #     ns.update(frame.f_locals)
    #     code.interact(local=ns)
