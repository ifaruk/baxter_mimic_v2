import math
import numpy


class Point(object):
    def __init__(self, x, y, z):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)
        self.direction = math.atan2(self.z, -self.x)
        self.distance = math.sqrt(self.x ** 2 + self.z ** 2)

    def get_vector(self):
        return (self.x, self.y, self.z)


class Focus_point(object):
    BEHAVIOR_CONSTANT_POINT = 0
    BEHAVIOR_FIND_CLOSEST = 1
    BEHAVIOR_FOLLOW_CLOSEST = 2
    BEHAVIOR_ANY_WHERE_BUT_IN_RANGE = 3

    def __init__(self, point, behavior=BEHAVIOR_CONSTANT_POINT, range=0):

        behaviors = {self.BEHAVIOR_CONSTANT_POINT        : Focus_point._b_constant_point,
                     self.BEHAVIOR_FIND_CLOSEST          : Focus_point._b_find_closest,
                     self.BEHAVIOR_FOLLOW_CLOSEST        : Focus_point._b_follow_closest,
                     self.BEHAVIOR_ANY_WHERE_BUT_IN_RANGE: Focus_point._b_any_where_but_in_range}

        assert (point is not None)
        assert (isinstance(point, Point))
        if behavior is not None:
            assert (behavior in behaviors.keys())
        else:
            behavior = behaviors[self.BEHAVIOR_CONSTANT_POINT]

        self._current = point
        self._behavior = behaviors[behavior]
        self.range = range

    def get_next_point(self, kinect_data, baxter_data):
        return self._behavior(self, kinect_data, baxter_data)

    def get_current_point(self):
        return self._current

    def _b_constant_point(self, kinect_data, baxter_data):
        return self._current

    def _b_find_closest(self, kinect_data, baxter_data):
        if kinect_data is not None and len(kinect_data.people) > 0:
            all_people = numpy.asarray([p.get_vector() for p in kinect_data.people])
            dist = numpy.sum((all_people - self._current.get_vector()) ** 2, axis=1)
            self._current = kinect_data.people[numpy.argmin(dist)]
        return self._current

    def _b_follow_closest(self, kinect_data, baxter_data):
        self._current = self._b_find_closest(kinect_data, baxter_data)
        return self._current

    def _b_any_where_but_in_range(self, kinect_data, baxter_data):
        # Find any other point but current point with range
        if kinect_data is not None and len(kinect_data.people) > 0:
            all_people = numpy.asarray([p.get_vector() for p in kinect_data.people])
            dist = numpy.sum((all_people - self._current.get_vector()) ** 2, axis=1)
            self._current = kinect_data.people[numpy.random.choice(numpy.where(dist > range))]
        return self._current


joint_sides = ['left', 'right']
joint_names = ['s0', 's1', 'e0', 'e1', 'w0', 'w1', 'w2']

joint_limits = {'s0': (-1.69, 1.69),
                's1': (-2.1, 1.0),
                'e0': (-3.0, 3.0),
                'e1': (0.0, 2.6),
                'w0': (-3.0, 3.0),
                'w1': (-1.5, 2.0),
                'w2': (-3.0, 3.0)}

joint_ranges = {'s0': 3.31,
                's1': 3.1,
                'e0': 6.0,
                'e1': 2.6,
                'w0': 6.0,
                'w1': 3.5,
                'w2': 6.0}

joint_speed_limits = {'s0': 2.0,
                      's1': 2.0,
                      'e0': 2.0,
                      'e1': 2.0,
                      'w0': 4.0,
                      'w1': 4.0,
                      'w2': 4.0}

joint_name_conversion = {'left' : 'right',
                         'right': 'left'}

joint_conversion_coeff = {'s0': -1.0,
                          's1': 1.0,
                          'e0': -1.0,
                          'e1': 1.0,
                          'w0': -1.0,
                          'w1': 1.0,
                          'w2': -1.0}


def get_symmetric(angles):
    ''':type angles:dict[str,float]'''
    assert (isinstance(angles, dict))
    analyse = [(key.split('_')[0], key.split('_')[1], angle) for key, angle in angles.items()]
    return dict([(joint_name_conversion[side] + '_' + joint, angle * joint_conversion_coeff[joint]) for side, joint, angle in analyse])


def get_joint_angle(side, s0=0.0, s1=0.0, e0=0.0, e1=0.0, w0=0.0, w1=0.0, w2=0.0):
    return {side + '_' + 'w2': w2,
            side + '_' + 'w1': w1,
            side + '_' + 'w0': w0,
            side + '_' + 'e1': e1,
            side + '_' + 'e0': e0,
            side + '_' + 's1': s1,
            side + '_' + 's0': s0}
