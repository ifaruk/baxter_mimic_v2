import bisect
import time
from itertools import chain

from basic_data import Point, Focus_point
from my_kinect import Kinect_data

from my_baxter import Baxter_data

baxter_point = Point(0, 2, 0)


class _Time(object):
    def __init__(self):
        self._arm_started = False
        self._head_started = False
        self._speech_started = False
        self._init_time = None

    def arm_ready(self):
        self._arm_started = True
        while not self._head_started or not self._speech_started:
            return False
        if self._init_time is None:
            self._init_time = time.time()
        return True

    def head_ready(self):
        self._head_started = True
        while not self._arm_started or not self._speech_started:
            return False
        if self._init_time is None:
            self._init_time = time.time()
        return True

    def speech_ready(self):
        self._speech_started = True
        while not self._arm_started or not self._head_started:
            return False
        if self._init_time is None:
            self._init_time = time.time()
        return True

    def get_current_time(self):
        if self._init_time is None:
            return 0
        return time.time() - self._init_time


class Simple_action(object):
    """
    :type focus_point:Focus_point
    """

    def __init__(self, focus_point):
        if focus_point is not None:
            assert (isinstance(focus_point, Focus_point))
        self.focus_point = focus_point

    def get_next(self, cur_time, kinect_data, baxter_data):
        raise AttributeError("get_next function is not implemented!")
        # assert (False)  # Every child class should implement this function

    @staticmethod
    def is_acceptable(focus_point, kinect_data, baxter_data, actions):
        # If an action has some conditions it will overridden in child
        return True


class Action_list_member(object):
    def __init__(self, action, start, finish):
        assert (isinstance(action, Simple_action))
        assert (isinstance(start, (float, int)))
        assert (isinstance(finish, (float, int)))
        assert (start <= finish)
        self.action = action
        self.start = float(start)
        self.finish = float(finish)


class Action(object):
    """
    An action consists of 3 list of Simple_actions head, arm and speech
    :type _speech_action_list:list[Action_list_member]
    :type _speech_action_timing:list[float]
    :type _head_action_list:list[Action_list_member]
    :type _head_action_timing:list[float]
    :type _arm_action_list:list[Action_list_member]
    :type _arm_action_timing:list[float]
    """

    def __init__(self, speech=None, head=None, arm=None):
        self._speech_action_list = None
        self._speech_action_timing = [0, 0]
        self._head_action_list = None
        self._head_action_timing = [0, 0]
        self._arm_action_list = None
        self._arm_action_timing = [0, 0]

        self.speech_action_list = speech
        self.head_action_list = head
        self.arm_action_list = arm
        self._time_sync = _Time()

    @staticmethod
    def _accept_list(input_list):
        if input_list is None:
            resulting_list = []
            resulting_timing = [0, 0]
        else:
            assert (isinstance(input_list, list))
            if len(input_list) == 0:
                resulting_list = []
                resulting_timing = [0, 0]
            else:
                assert (isinstance(input_list[0], Action_list_member))
                resulting_list = input_list
                resulting_timing = list(chain.from_iterable((a.start, a.finish) for a in input_list))
                assert (sorted(resulting_timing) == resulting_timing)
        return resulting_list, resulting_timing

    def speech_action_list(self, speech):
        self._speech_action_list, self._speech_action_timing = self._accept_list(speech)

    def head_action_list(self, head):
        self._head_action_list, self._head_action_timing = self._accept_list(head)

    def arm_action_list(self, arm):
        self._arm_action_list, self._arm_action_timing = self._accept_list(arm)

    speech_action_list = property(None, speech_action_list)
    head_action_list = property(None, head_action_list)
    arm_action_list = property(None, arm_action_list)

    def is_speech_action_ready(self):
        return self._time_sync.speech_ready()

    def is_head_action_ready(self):
        return self._time_sync.head_ready()

    def is_arm_action_ready(self):
        return self._time_sync.arm_ready()

    # ----------------------------------------------------------------------------------
    def _get_action_list_member_at_time(self, time, action_timing, action_list):
        '''
        :type action_list: list[Action_list_member]
        :rtype: Action_list_member | None
        '''
        action = bisect.bisect_right(action_timing, time)
        if action % 2 == 0:
            # We should not move
            return None
        else:
            # Remove waiting actions find correct order from list
            action = (action - 1) / 2
            return action_list[action]

    # ----------------------------------------------------------------------------------
    def get_speech_action_at_time(self, time):
        '''
        :rtype :Simple_action | None
        '''
        a = self._get_action_list_member_at_time(time, self._speech_action_timing, self._speech_action_list)
        if a is None:
            return None
        return a.action

    def get_head_action_at_time(self, time):
        '''
        :rtype :Simple_action | None
        '''
        a = self._get_action_list_member_at_time(time, self._head_action_timing, self._head_action_list)

        if a is None:
            return None
        return a.action

    def get_arm_action_at_time(self, time):
        '''
        :rtype :Simple_action | None
        '''
        a = self._get_action_list_member_at_time(time, self._arm_action_timing, self._arm_action_list)

        if a is None:
            return None
        return a.action

    # ----------------------------------------------------------------------------------
    def get_speech_action_current(self):
        '''
        :rtype :Simple_action | None
        '''
        return self.get_speech_action_at_time(self._time_sync.get_current_time())

    def get_head_action_current(self):
        '''
        :rtype :Simple_action | None
        '''
        return self.get_head_action_at_time(self._time_sync.get_current_time())

    def get_arm_action_current(self):
        '''
        :rtype :Simple_action | None
        '''
        return self.get_arm_action_at_time(self._time_sync.get_current_time())

    # ----------------------------------------------------------------------------------
    def get_actions_at_time(self, time):
        return (self.get_speech_action_at_time(time),
                self.get_head_action_at_time(time),
                self.get_arm_action_at_time(time))

    def get_actions_at_current(self):
        return (self.get_speech_action_current(),
                self.get_head_action_current(),
                self.get_arm_action_current())

    # ----------------------------------------------------------------------------------
    def get_speech_focus_point_at_time(self, time):
        '''
        :rtype: Point | None 
        '''
        a = self.get_speech_action_at_time(time)
        if a is not None and a.focus_point is not None:
            return a.focus_point.get_current_point()
        else:
            return None

    def get_head_focus_point_at_time(self, time):
        '''
        :rtype: Point | None 
        '''
        a = self.get_head_action_at_time(time)
        if a is not None and a.focus_point is not None:
            return a.focus_point.get_current_point()
        else:
            return None

    def get_arm_focus_point_at_time(self, time):
        '''
        :rtype: Point | None 
        '''
        a = self.get_arm_action_at_time(time)
        if a is not None and a.focus_point is not None:
            return a.focus_point.get_current_point()
        else:
            return None

    # ----------------------------------------------------------------------------------
    def get_speech_focus_point_current(self):
        '''
        :rtype: Point | None 
        '''
        a = self.get_speech_action_current()
        if a is not None and a.focus_point is not None:
            return a.focus_point.get_current_point()
        else:
            return None

    def get_head_focus_point_current(self):
        '''
        :rtype: Point | None 
        '''
        a = self.get_head_action_current()
        if a is not None and a.focus_point is not None:
            return a.focus_point.get_current_point()
        else:
            return None

    def get_arm_focus_point_current(self):
        '''
        :rtype: Point | None 
        '''
        a = self.get_arm_action_current()
        if a is not None and a.focus_point is not None:
            return a.focus_point.get_current_point()
        else:
            return None

    # ----------------------------------------------------------------------------------
    def _get_new_values(self, action_timing, action_list, kinect_data, baxter_data):
        '''
        :
        :type action_timing: list[float]
        :type action_list:  list[Action_list_member]
        :type  kinect_data: Kinect_data
        :type  baxter_data: Baxter_data

        '''
        action_lm = self._get_action_list_member_at_time(self._time_sync.get_current_time(),
                                                         action_timing,
                                                         action_list)
        if action_lm is not None:
            return action_lm.action.get_next(self._time_sync.get_current_time() - action_lm.start,
                                             kinect_data,
                                             baxter_data)
        else:
            return None

    # ----------------------------------------------------------------------------------
    def speech_get_new_values(self, kinect_data, baxter_data):
        assert(isinstance(kinect_data,Kinect_data))
        assert(isinstance(baxter_data,Baxter_data))
        return self._get_new_values(self._speech_action_timing, self._speech_action_list, kinect_data, baxter_data)

    def head_get_new_values(self, kinect_data, baxter_data):
        assert (isinstance(kinect_data, Kinect_data))
        assert (isinstance(baxter_data, Baxter_data))
        return self._get_new_values(self._head_action_timing, self._head_action_list, kinect_data, baxter_data)

    def arm_get_new_values(self, kinect_data, baxter_data):
        assert (isinstance(kinect_data, Kinect_data))
        assert (isinstance(baxter_data, Baxter_data))
        return self._get_new_values(self._arm_action_timing, self._arm_action_list, kinect_data, baxter_data)

    def get_duration(self):
        return max([self._speech_action_timing[-1],
                    self._head_action_timing[-1],
                    self._arm_action_timing[-1]])

    def get_remaining_time(self):
        return self.get_duration() - self._time_sync.get_current_time()

    def is_completed(self):
        return self.get_remaining_time() < 0
