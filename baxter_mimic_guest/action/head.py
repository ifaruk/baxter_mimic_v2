import bisect
import math
import random
from os import walk

import numpy
from action.actions import Simple_action as _Simple_action

from tools import translate as _translate

hfd = 'face'
hfd_tuple = [i for i in walk(hfd)]
''' :type : (str,list[str],list[str])'''
_hfile_names = hfd_tuple[0][2]

fl_talks = [hfd + '/' + f for f in _hfile_names if f.startswith('speak')]
fl_normals = [hfd + '/'+ f for f in _hfile_names if f.startswith('normal')]
fl_laughing = [hfd + '/' + f for f in _hfile_names if f.startswith('laughing')]

f_in_peace_eyes_closed = hfd + '/in_peace_eyes_closed.jpg'
f_little_worried_eyes_straight = hfd + '/little_worried_eyes_straight.jpg'
f_little_worried_eyes_up = hfd + '/little_worried_eyes_up.jpg'

f_normal_eyes_down = hfd + '/normal_eyes_down.jpg'
f_normal_eyes_down_left = hfd + '/normal_eyes_down_left.jpg'
f_normal_eyes_down_right = hfd + '/normal_eyes_down_right.jpg'
f_normal_eyes_left = hfd + '/normal_eyes_left.jpg'
f_normal_eyes_right = hfd + '/normal_eyes_right.jpg'
f_normal_eyes_straight = hfd + '/normal_eyes_straight.jpg'
f_normal_eyes_up = hfd + '/normal_eyes_up.jpg'
f_normal_eyes_up_left = hfd + '/normal_eyes_up_left.jpg'
f_normal_eyes_up_right = hfd + '/normal_eyes_up_right.jpg'

f_sleeping = hfd + '/sleeping.jpg'
f_yawning = hfd + '/yawning.jpg'

_head_angle_min = -1.38
_head_angle_max = 1.38


class _Generic_head_action(_Simple_action):
    def __init__(self, possible_faces, period, no_changes, focus_point):
        assert (isinstance(period, (float, int)))
        assert (isinstance(no_changes, int))
        period = float(period)
        super(_Generic_head_action, self).__init__(focus_point)

        self.angle = numpy.random.normal(-0.1, 0.1, no_changes)
        self.speed = numpy.clip(numpy.abs(numpy.random.normal(0, 0.5, no_changes)), 0, 1)
        self.times = numpy.sort(numpy.random.uniform(0.0, period / float(no_changes), no_changes) * no_changes)
        self.times += [period]
        self.face = [random.choice(possible_faces) for i in range(no_changes)]

    def get_next(self, cur_time, kinect_data, baxter_data):
        '''
        :type kinect_data:Kinect_data
        :type baxter_data:Baxter_data
        :return: 
        '''
        # if kinect_data is not None:
        #     assert (isinstance(kinect_data, _Kinect_data))
        # else:
        #     return (None, None, None)

        cur_time %= self.times[-1]
        step = bisect.bisect_right(self.times, cur_time) - 1
        follow = self.focus_point.get_next_point(kinect_data, baxter_data)
        head_direction = _translate(follow.direction, 0, math.pi, _head_angle_min, _head_angle_max)
        head_direction += self.angle[step]
        head_angle = max(min(head_direction, _head_angle_max), _head_angle_min)
        return (head_angle, self.speed[step], self.face[step])


class Neutral(_Generic_head_action):
    def __init__(self, focus_point):
        super(Neutral, self).__init__([f_normal_eyes_straight],
                                      600,
                                      1,
                                      focus_point)


class Happy(_Generic_head_action):
    def __init__(self, focus_point):
        super(Happy, self).__init__(fl_laughing + fl_normals,
                                    20.0,
                                    3,
                                    focus_point)



class Looking_around(_Generic_head_action):
    def __init__(self, focus_point):
        super(Looking_around, self).__init__(fl_normals,
                                      3.0,
                                      10,
                                      focus_point)

# class Looking_around(_Simple_action):
#     def __init__(self, focus_point):
#         super(Looking_around, self).__init__(focus_point)
# 
# 
#     def get_next(self, cur_time, kinect_data, baxter_data):
#         '''
#         :type kinect_data:Kinect_data
#         :type baxter_data:Baxter_data
#         :return: 
#         '''
#         # if kinect_data is not None:
#         #     assert (isinstance(kinect_data, _Kinect_data))
#         # else:
#         #     return (None, None, None)
# 
#         cur_time %= self.times[-1]
#         step = bisect.bisect_right(self.times, cur_time) - 1
#         follow = self.focus_point.get_next_point(kinect_data, baxter_data)
#         head_direction = _translate(follow.direction, 0, math.pi, _head_angle_min, _head_angle_max)
#         head_direction += self.angle[step]
#         head_angle = max(min(head_direction, _head_angle_max), _head_angle_min)
#         return (head_angle, self.speed[step], self.face[step])



class Talking(_Simple_action):
    def __init__(self,focus_point):
        period = 5.0
        super(Talking, self).__init__(focus_point)

        self.angle = numpy.random.normal(-0.1, 0.1, len(fl_talks))
        self.times = [i for i in numpy.arange(0,period,period/float(len(fl_talks)))]
        self.times += [period]
        self.face = fl_talks

    def get_next(self, cur_time, kinect_data, baxter_data):
        '''
        :type kinect_data:Kinect_data
        :type baxter_data:Baxter_data
        :return: 
        '''
        # if kinect_data is not None:
        #     assert (isinstance(kinect_data, _Kinect_data))
        # else:
        #     return (None, None, None)

        cur_time %= self.times[-1]
        step = bisect.bisect_right(self.times, cur_time) - 1
        follow = self.focus_point.get_next_point(kinect_data, baxter_data)
        head_direction = _translate(follow.direction, 0, math.pi, _head_angle_min, _head_angle_max)
        head_direction += self.angle[step]
        head_angle = max(min(head_direction, _head_angle_max), _head_angle_min)
        return (head_angle, 0.1, self.face[step])


class Talking_wait(_Generic_head_action):
    def __init__(self, focus_point):
        super(Talking_wait, self).__init__(
                [f_normal_eyes_straight],
                10.0,
                1,
                focus_point)


class Sleep(_Generic_head_action):
    def __init__(self, focus_point):
        super(Sleep, self).__init__([f_sleeping],
                                    20.0,
                                    1,
                                    focus_point)
