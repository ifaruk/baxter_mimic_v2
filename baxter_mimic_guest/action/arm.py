import bisect
import copy
import math

import numpy
from action.actions import Simple_action
import conf
from my_kinect import Kinect_data
from tools import translate

import basic_data as bd
from basic_data import Point, Focus_point, get_symmetric, get_joint_angle

# ----------------------------------------------------------------------------------------------

do_natural_left_angles = get_joint_angle('left', 1.147, 0.07, -0.67, 2.286, -0.565, -0.73, -0.903)
do_natural_right_angles = get_symmetric(do_natural_left_angles)

# ----------------------------------------------------------------------------------------------

do_straight_arm_left = get_joint_angle('left', -math.pi / 4, 0, -math.pi, 0, 0, 0, 0)
do_straight_arm_right = get_symmetric(do_straight_arm_left)

# ----------------------------------------------------------------------------------------------

do_palm_up_near_shoulder_left = get_joint_angle('left', -math.pi / 4 + 1.4, 0.5, -math.pi, 2.0, 0.0, -1.0, 0.0)
do_palm_up_near_shoulder_right = get_symmetric(do_palm_up_near_shoulder_left)

various_left_poses = [get_joint_angle('left', -0.418, 0.129, 0.378, 0.538, 0.477, 0.0, 0.0),
                      get_joint_angle('left', -0.279, 0.976, -1.020, 1.172, 2.894, 0.0, 0.0),
                      get_joint_angle('left', 0.335, 0.088, -0.190, 0.717, -0.454, 0.0, 0.0),
                      get_joint_angle('left', 0.301, 0.013, 2.838, 2.300, 2.040, 0.0, 0.0),
                      get_joint_angle('left', 0.699, -0.403, -2.175, 0.978, 0.921, 0.0, 0.0),
                      get_joint_angle('left', 0.611, -0.191, -0.982, 1.263, -0.170, 0.0, 0.0),
                      get_joint_angle('left', -0.229, -0.416, -1.276, 1.295, 1.284, 0.0, 0.0),
                      get_joint_angle('left', -0.248, -0.484, -1.181, 1.178, 0.065, 0.0, 0.0),
                      get_joint_angle('left', -0.10, -0.416, -1.276, 1.295, 1.284, 0.0, 0.0),
                      get_joint_angle('left', -0.10, -0.484, -1.181, 1.178, 0.065, 0.0, 0.0),
                      get_joint_angle('left', 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)]


# ----------------------------------------------------------------------------------------------

def choose_right_arm(angle, was_using_right):
    flexibility = math.pi / 10

    if was_using_right is True:
        if 0.0 <= angle <= (math.pi / 2 + flexibility):
            return True
        elif (math.pi / 2 + flexibility) <= angle <= math.pi:
            return False
        else:
            return None
    elif was_using_right is False:
        if 0.0 <= angle <= (math.pi / 2 - flexibility):
            return True
        elif (math.pi / 2 - flexibility) <= angle <= math.pi:
            return False
        else:
            return None
    else:
        if 0.0 <= angle <= math.pi / 2:
            return True
        elif math.pi / 2 <= angle <= math.pi:
            return False
        else:
            return None


straight_focus = Focus_point(Point(0,
                                   2.0,
                                   conf.person_of_interest_z))


class Neutral(Simple_action):
    def __init__(self, focus_point):
        super(Neutral, self).__init__(focus_point)

    def get_next(self, cur_time, kinect_data, baxter_data):
        return (do_natural_left_angles, do_natural_right_angles)


class Stop(Simple_action):
    def __init__(self, focus_point):
        super(Stop, self).__init__(focus_point)

    def get_next(self, cur_time, kinect_data, baxter_data):
        return (None, None)


class Safety(Simple_action):
    def __init__(self, focus_point):
        super(Safety, self).__init__(focus_point)

    def get_next(self, cur_time, kinect_data, baxter_data):
        return (bd.get_joint_angle('left', s1=0.5), bd.get_joint_angle('right', s1=0.5))


class Wave(Simple_action):
    waving_left_0 = get_joint_angle('left', -0.4, 0.61, -2.8, 1.73, 2.65, -0.47, 0.0)
    waving_left_1 = get_joint_angle('left', -0.32, 0.62, -2.4, 2.0, 3.0, -0.31, 0.0)

    points = [waving_left_0, waving_left_1]
    update_functions = [lambda direction: -translate(direction, math.pi / 2, math.pi, 0.6, -0.5),
                        lambda direction: -0.1 - translate(direction, math.pi / 2, math.pi, 0.6, -0.5)]
    times = [0.0, 1.0, 2.0]

    def __init__(self, focus_point):
        super(Wave, self).__init__(focus_point)
        self.right_active = None

    def get_next(self, cur_time, kinect_data, baxter_data):
        '''
        :param cur_time: 
        :param kinect_data: 
        :param baxter_data: 
        :return: 
        
        :type kinect_data:Kinect_data
        
        '''
        if kinect_data is not None:
            assert (isinstance(kinect_data, Kinect_data))
        else:
            return (None, None)

        follow = self.focus_point.get_next_point(kinect_data, baxter_data)

        cur_time %= self.times[-1]
        point = bisect.bisect_right(self.times, cur_time) - 1

        cra = choose_right_arm(follow.direction, self.right_active)
        if cra is True:
            self.right_active = True
            # wave right
            lp = copy.deepcopy(self.points[point])
            # convert into z axis symmetric to get direction as if it is on left side
            cur_direction_in_left = translate(follow.direction, 0, math.pi / 2, math.pi, math.pi / 2)
            lp['left_s0'] = self.update_functions[point](cur_direction_in_left)
            right = get_symmetric(lp)

            # natural left
            left = do_natural_left_angles

            return (left, right)

        elif cra is False:
            self.right_active = False
            # wave right
            right = do_natural_right_angles

            # natural left
            left = copy.deepcopy(self.points[point])
            left['left_s0'] = self.update_functions[point](follow.direction)

            return (left, right)
        else:
            self.right_active = None
            return (None, None)

    @staticmethod
    def is_acceptable(focus_point, kinect_data, baxter_data, actions):
        '''
        
        :type focus_point: Focus_point 
        :param kinect_data: Kinect_data
        :param baxter_data: Baxter_data
        :param actions: 
        :return: 
        '''
        # If an action has some conditions it will overridden in child
        if focus_point.range < conf.poi_focus.range + 1.0:
            return True
        return False


class Pointing(Simple_action):
    def __init__(self, focus_point):
        super(Pointing, self).__init__(focus_point)
        self.right_active = None

    def get_next(self, cur_time, kinect_data, baxter_data):
        '''
        :param cur_time: 
        :param baxter_data: 
        :param kinect_data: 
        :type kinect_data:Kinect_data
        :return: 
        '''
        if kinect_data is not None:
            assert (isinstance(kinect_data, Kinect_data))
        else:
            return (None, None)

        follow = self.focus_point.get_next_point(kinect_data, baxter_data)

        cra = choose_right_arm(follow.direction, self.right_active)

        if cra is True:
            self.right_active = True
            # wave right
            lp = copy.deepcopy(do_straight_arm_left)
            # convert into z axis symmetric to get direction as if it is on left side
            cur_direction_in_left = translate(follow.direction, 0, math.pi / 2, math.pi, math.pi / 2)
            # Scale and translate angle to baxter angles
            lp['left_s0'] = -translate(cur_direction_in_left, math.pi / 2, math.pi, 0.9, -0.5)
            lp['left_e0'] = -translate(cur_direction_in_left, math.pi / 2, math.pi, 0, -2.0)
            right = get_symmetric(lp)

            # natural left
            left = do_natural_left_angles

            return (left, right)

        elif cra is False:
            self.right_active = False
            right = do_natural_right_angles

            left = copy.deepcopy(do_straight_arm_left)
            # Scale and translate angle to baxter angles
            left['left_s0'] = -translate(follow.direction, math.pi / 2, math.pi, 0.9, -0.5)
            left['left_e0'] = -translate(follow.direction, math.pi / 2, math.pi, 0, -2.0)

            return (left, right)
        else:
            self.right_active = None
            return (None, None)

    @staticmethod
    def is_acceptable(focus_point, kinect_data, baxter_data, actions):
        '''

        :type focus_point: Focus_point 
        :param kinect_data: Kinect_data
        :param baxter_data: Baxter_data
        :param actions: 
        :return: 
        '''
        # If an action has some conditions it will overridden in child
        if focus_point.range < conf.poi_focus.range + 1.0:
            return True
        return False


class Mimic(Simple_action):
    def __init__(self, focus_point):
        super(Mimic, self).__init__(focus_point)

    def get_next(self, cur_time, kinect_data, baxter_data):
        if kinect_data is None or kinect_data.pos_ja is None:
            return (None, None)
        return kinect_data.pos_ja


class Mimic_hard(Simple_action):
    def __init__(self, focus_point):
        super(Mimic_hard, self).__init__(focus_point)

    def get_next(self, cur_time, kinect_data, baxter_data):
        if kinect_data is None or kinect_data.pos_ja is None:
            return (None, None)
        return (get_symmetric(kinect_data.pos_ja[1]), get_symmetric(kinect_data.pos_ja[0]))


class Do(Simple_action):
    def __init__(self, left=do_natural_left_angles, right=do_natural_right_angles):
        super(Do, self).__init__(straight_focus)
        self.left = left
        self.right = right

    def get_next(self, cur_time, kinect_data, baxter_data):
        return (self.left, self.right)


class Two_hands_palms_up_wondering(Simple_action):
    def __init__(self, focus_point):
        super(Two_hands_palms_up_wondering, self).__init__(focus_point)

    def get_next(self, cur_time, kinect_data, baxter_data):
        return (do_palm_up_near_shoulder_left, do_palm_up_near_shoulder_right)


class Two_hand_up_waving(Simple_action):
    left_wp_0 = bd.get_joint_angle('left', 1.0, -0.6, -3.0, 1.7, 1.3)
    left_wp_1 = bd.get_joint_angle('left', 1.0, -0.9, -3.0, 0.5, 1.3)
    left_points = [left_wp_0, left_wp_1]
    times = [0.0, 1.0, 2.0]

    def __init__(self, focus_point):
        super(Two_hand_up_waving, self).__init__(focus_point)

    def get_next(self, cur_time, kinect_data, baxter_data):
        cur_time %= self.times[-1]
        point = bisect.bisect_right(self.times, cur_time) - 1
        return (self.left_points[point], get_symmetric(self.left_points[point]))

    @staticmethod
    def is_acceptable(focus_point, kinect_data, baxter_data, actions):
        '''

        :type focus_point: Focus_point 
        :param kinect_data: Kinect_data
        :param baxter_data: Baxter_data
        :param actions: 
        :return: 
        '''
        # If an action has some conditions it will overridden in child
        if focus_point.range < conf.poi_focus.range + 1.0:
            return True
        return False


class Random(Simple_action):
    def __init__(self, focus_point):
        super(Random, self).__init__(focus_point)
        self.current = None

    def get_next(self, cur_time, kinect_data, baxter_data):
        if cur_time % 5 == 0:
            self.current = None
        if self.current is None:
            self.current = {s: get_joint_angle(s, **dict([(jn, numpy.random.uniform(bd.joint_limits[jn][0], bd.joint_limits[jn][1])) for jn in bd.joint_names]))
                            for s in bd.joint_sides}
        return (self.current['left'], self.current['right'])


class About_to_sleep(Simple_action):
    left_wp_0 = bd.get_joint_angle('left', 1.147, 0.07, -0.67, 2.286, -0.565, -0.73, -0.903)
    left_wp_1 = bd.get_joint_angle('left', 0.52, 0.75, -1.72, 2.28, -0.13, 0.96, 0.56)
    left_wp_2 = bd.get_joint_angle('left', 0.95, 0.25, -2.33, 1.7, -0.55, 1.11, 0.46)
    left_wp_3 = bd.get_joint_angle('left', 0.47, -0.65, -2.61, 1.13, -1.12, 0.65, 0.48)
    left_wp_4 = bd.get_joint_angle('left', 0.61, 0.02, -1.92, 1.55, 0.53, 1.47, -0.23)
    left_wp_5 = bd.get_joint_angle('left', 0.64, 0.86, -0.15, 0.67, 0.26, 0.0, -0.14)
    left_points = [left_wp_0, left_wp_1, left_wp_2, left_wp_3, left_wp_4, left_wp_5]
    times = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0]

    def __init__(self, focus_point):
        super(About_to_sleep, self).__init__(None)

    def get_next(self, cur_time, kinect_data, baxter_data):
        if cur_time <= self.times[-1]:
            point = bisect.bisect_right(self.times, cur_time) - 1
            return (self.left_points[point], get_symmetric(self.left_points[point]))
        else:
            return (self.left_points[-1], get_symmetric(self.left_points[-1]))

    @staticmethod
    def is_acceptable(focus_point, kinect_data, baxter_data, actions):
        '''

        :type focus_point: Focus_point 
        :param kinect_data: Kinect_data
        :param baxter_data: Baxter_data
        :param actions: 
        :return: 
        '''
        # If an action has some conditions it will overridden in child
        if focus_point.range < conf.poi_focus.range + 1.0:
            return True
        return False

# class Neutral(Simple_action):
#     def __init__(self, focus_point):
#         super(Stop, self).__init__(focus_point)
#
#     def get_next(self, cur_time, kinect_data, baxter_data):
#         return (natural_left_angles, natural_right_angles)
#
#     def get_duration(self):
#         return float_info.max
#
#
# class Neutral(Simple_action):
#     def __init__(self, focus_point):
#         super(Stop, self).__init__(focus_point)
#
#     def get_next(self, cur_time, kinect_data, baxter_data):
#         return (natural_left_angles, natural_right_angles)

#
# def arm_get_two_hand_wave_everyone(time):
#     '''
#
#     :param time:
#     :return:
#     '''
#
#     return []
#
#
# def arm_get_two_hang(time):
#     '''
#
#     :param time:
#     :return:
#     '''
#
#     return []
#
#
# def arm_get_two_hang_and_wave(time):
#     '''
#     :param time:
#     :return:
#     '''
#
#     return []
