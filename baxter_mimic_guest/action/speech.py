from action.actions import Simple_action
from sys import float_info
import wave
import contextlib
from os import walk


sfd = 'speech' #speech folder directory
sfd_tuple  = [i for i in walk(sfd)]
''' :type : (str,list[str],list[str])'''
_sfile_names = sfd_tuple[0][2]

# @file_start
# Good Morning


# Spending time
spending_time = [sfd + '/' +f for f in _sfile_names if f.startswith('attract')]

speak_se_alien_robot_0 = "speech/se_alien_robot_0.wav"
speak_se_creepy_robot_count_down_0 = "speech/se_creepy_robot_count_down_0.wav"
speak_se_dispatch_radio_0 = "speech/se_dispatch_radio_0.wav"
speak_se_electronic_confusion_0 = "speech/se_electronic_confusion_0.wav"
speak_se_evil_robot_access_denied_0 = "speech/se_evil_robot_access_denied_0.wav"
speak_se_evil_robot_laugh_0 = "speech/se_evil_robot_laugh_0.wav"
speak_se_female_robot_count_down_0 = "speech/se_female_robot_count_down_0.wav"
speak_se_gasp_0 = "speech/se_gasp_0.wav"
speak_se_giggle_boy_0 = "speech/se_giggle_boy_0.wav"
speak_se_male_robot_game_over_0 = "speech/se_male_robot_game_over_0.wav"
speak_se_monitor_beep_0 = "speech/se_monitor_beep_0.wav"
speak_se_radio_channel_change_0 = "speech/se_radio_channel_change_0.wav"
speak_se_radio_channel_change_1 = "speech/se_radio_channel_change_1.wav"
speak_se_radio_static_0 = "speech/se_radio_static_0.wav"
speak_se_robot_game_over_0 = "speech/se_robot_game_over_0.wav"
speak_se_robot_shut_down_0 = "speech/se_robot_shut_down_0.wav"
speak_se_robot_talking_with_beeps_0 = "speech/se_robot_talking_with_beeps_0.wav"
speak_se_robot_wakeup_0 = "speech/se_robot_wakeup_0.wav"
speak_se_scream_0 = "speech/se_scream_0.wav"
speak_se_scream_female_0 = "speech/se_scream_female_0.wav"
speak_se_self_destruction_0 = "speech/se_self_destruction_0.wav"
speak_se_sneeze_0 = "speech/se_sneeze_0.wav"
speak_se_sneeze_1 = "speech/se_sneeze_1.wav"
speak_se_snore_0 = "speech/se_snore_0.wav"
speak_se_yawn_0 = "speech/se_yawn_0.wav"
speak_se_yawn_male_0 = "speech/se_yawn_male_0.wav"

##Jokes


# Leaving Person

leave_early = [sfd + '/' +f for f in _sfile_names if f.startswith('leaveEarly')]

# Greet and Explain Game

intro = [sfd + '/' +f for f in _sfile_names if f.startswith('intro')]

# Play Game 1


# Ready Game 2
your_turn = [sfd + '/' +f for f in _sfile_names if f.startswith('yourTurn')]

# Play Game 2


# Thank and wait for next
good_game_result = [sfd + '/' +f for f in _sfile_names if f.startswith('finishGood')]
bad_game_result = [sfd + '/' +f for f in _sfile_names if f.startswith('finishBad')]

# Good Night


speak_silence = None


# @file_start


class Speak(Simple_action):
    def __init__(self, file, repeat=False, repeat_period=0.0):
        super(Speak, self).__init__(None)
        assert (isinstance(repeat, bool))
        if file is not None:
            assert (isinstance(file, str))
        assert (isinstance(repeat_period, float))
        self.file = file
        self.repeat = repeat
        self.repeat_period = repeat_period

        if file is not None:
            with contextlib.closing(wave.open(self.file, 'r')) as f:
                frames = f.getnframes()
                rate = f.getframerate()
                self.duration = frames / float(rate)
        else:
            self.duration=0.1


    def get_next(self, cur_time, kinect_data, baxter_data):
        if not self.repeat:
            if 0 <= cur_time <= self.duration:
                return self.file
            else:
                return None
        else:
            if 0 <= (cur_time % self.repeat_period) <= self.duration:
                return self.file
            else:
                return None
